package de.tarent.oprecoserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import org.eclipse.swt.widgets.Display;


public class SocketServerTransport implements ServerTransport {

	static SocketServerTransport socketServerTransport;
	
	int port;
	ServerSocket serverSocket;
	Socket currentSocket;
	boolean sockethandlingboolean[] = new boolean[100];
	int socketCount = 1;
	boolean daemonMode = false;
	
	
	static String password = "";
	static String randomNumber = "";
	static String md5Password = "";
	String currentClient = "No client connected";
	
	
	ArrayList<String> logList = new ArrayList<String>();
	ArrayList<String> clientList = new ArrayList<String>();


	public void addlog(String message) {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss ");
		Date currentTime = new Date();
		logList.add(formatter.format(currentTime) + ": " + message);
		System.out.println(formatter.format(currentTime) + ": " + message);
	}

	public void addClientList(String message) {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss ");
		Date currentTime = new Date();
		clientList.add(formatter.format(currentTime) + ": " + message);
	}

	private void btlisten() {
		new Thread(new Runnable() {
			public void run() {
				try {
					Runtime.getRuntime().exec(
							"/usr/share/java/opreco/pand-server");
					addlog("Setting up PAN Server");
				} catch (IOException e) {
					addlog("Error on pand --listen");
					e.printStackTrace();
				}
			}
		}).start();
	}
	
	public boolean readPasswordFromFile() throws IOException{
		File passwordFile = new File(Main.homepath+"/password.settings");
		if(!passwordFile.exists())
			return false;
		BufferedReader passwordReader = new BufferedReader(new FileReader(passwordFile));
		randomNumber = passwordReader.readLine();
		md5Password = passwordReader.readLine();
		return true;
		
	}
	
	public void generatePassword(String passwordtmp){
		randomNumber = String.valueOf(Math.floor(Math.random() * 100) + 1);
		md5Password = ChecksumTool.createChecksum(passwordtmp + randomNumber);
	}

	public synchronized void listen(CommandHandler handler) throws IOException {
		addlog("opening server");
		boolean askForPassword = false;
		
		if(!readPasswordFromFile() && !daemonMode)
			askForPassword = true;
		if (!daemonMode)
			Gui.getInstance().trayIcon(askForPassword);
		serverSocket = new ServerSocket(port);
		if(askForPassword)
			generatePassword(password);
		
		btlisten();

		// accept any connection at a time

		while (!serverSocket.isClosed()) {
			addlog("accepting on port " + port);
			Socket socket = new Socket();
			socket = serverSocket.accept();
			addlog("got connection from " + socket.getRemoteSocketAddress());
			addClientList("" + socket.getRemoteSocketAddress());
			OutputStreamWriter writer;
			writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
			BufferedReader reader;
			reader = new BufferedReader(new InputStreamReader(socket
					.getInputStream()));

			//String random = String.valueOf(Math.floor(Math.random() * 100) + 1);
			writer.write(randomNumber);
			writer.write("\n");
			writer.flush();

			//String md5 = ChecksumTool.createChecksum(password + random);
			System.out.println(randomNumber);
			System.out.println(md5Password);

			String incommingMD5 = null;
			incommingMD5 = reader.readLine();
			if (incommingMD5.equals(md5Password)) {
				SocketHandling socketHandling = new SocketHandling(handler,
						socket, socketCount);
				socketCount++;
				if(!daemonMode)
					Gui.getInstance().showToolTipNewClient();
				close();
			} else {
				writer.write("Bad Password");
				writer.write("\n");
				writer.flush();
				if(!daemonMode)
					Gui.getInstance().showToolTipBadPassword();
			}
		}
	}

	public void close() throws IOException {
		String bnep0 = "";
		String bnep1 = "";
		String macAdressToKick = "";
		
		Process getInterfaceToKick = Runtime.getRuntime().exec("pand --show");
		BufferedReader getInterfaceToKickInputReader = new BufferedReader(new InputStreamReader(getInterfaceToKick.getInputStream()));
		
		String[] interfaces = new String[2];
		interfaces[0] = getInterfaceToKickInputReader.readLine();
		interfaces[1] = getInterfaceToKickInputReader.readLine();
		
		
		for(int i = 0; i<2; i++){
			System.out.println(i);
			if(!(interfaces[i] == null) && interfaces[i].contains("bnep0")){
				StringTokenizer st = new StringTokenizer(interfaces[i]);
				st.nextToken();
				bnep0 = st.nextToken();
			}
		}
		for(int i = 0; i<2; i++){
			if(!(interfaces[i] == null) && interfaces[i].contains("bnep1")){
				StringTokenizer st = new StringTokenizer(interfaces[i]);
				st.nextToken();
				bnep1 = st.nextToken();
			}
		}	
		
		if((currentClient.substring(1, 11)).equals("172.16.0.2")){
			macAdressToKick = bnep1;
		}
		else if((currentClient.substring(1, 9)).equals("10.0.0.2")){
			macAdressToKick = bnep0;
		}	
		Runtime.getRuntime().exec("/usr/share/java/opreco/pand-kill "+macAdressToKick);
	}


	class SocketHandling {
		CommandHandler commandHandler;

		public SocketHandling(CommandHandler handler, final Socket socket,
				final int socketCount) {
			commandHandler = handler;
			new Thread(new Runnable() {
				public void run() {
					sockethandlingboolean[socketCount - 1] = false;
					sockethandlingboolean[socketCount] = true;
					addlog("SocketCount: " + socketCount);
					currentClient = "" + socket.getRemoteSocketAddress();
					if (!daemonMode) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								Gui.getInstance().setSubItemText(currentClient);
							}
						});
					}
					OutputStreamWriter writer = null;
					BufferedReader reader = null;
					try {
						writer = new OutputStreamWriter(socket
								.getOutputStream(), "UTF-8");
						reader = new BufferedReader(new InputStreamReader(
								socket.getInputStream()));
						String cmd;
						while (null != (cmd = reader.readLine())
								&& sockethandlingboolean[socketCount]) {
							addlog("got command: " + cmd);
							addlog("Command from: "
									+ socket.getRemoteSocketAddress());
							String result = commandHandler.processCommand(cmd);
							if (result == null) {
								addlog("send result: " + RESULT_OK);
								writer.write(RESULT_OK);
							} else {
								addlog("send result: " + result);
								writer.write(result);
							}
							writer.write("\n");
							writer.flush();
						}
						if (!sockethandlingboolean[socketCount]) {
							writer.write("kicked, new client");
							writer.write("\n");
							writer.flush();
							if (!socket.isClosed()) {
								addlog("Connection to "
										+ socket.getRemoteSocketAddress()
										+ " closed");
								socket.close();
							}
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();
		}

	}
	
	public void setDaemonMode(boolean daemonModetmp){
		this.daemonMode = daemonModetmp;
	}

	public boolean isConnected() {
		return currentSocket != null && currentSocket.isConnected();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public boolean[] getSocketHandlingBoolean(){
		return sockethandlingboolean;
	}
	
	public int getSocketCount(){
		return socketCount;
	}
	
	public ArrayList<String> getClientList(){
		return clientList;
	}
	
	public ArrayList<String> getLogList(){
		return logList;
	}
	
	public ServerSocket getServerSocket(){
		return serverSocket;
	}
	
	public void setPassword(String passwordtmp){
		this.password = passwordtmp;
	}
		
	public static SocketServerTransport getInstance(){
		if(socketServerTransport == null)
			socketServerTransport = new SocketServerTransport();
		return socketServerTransport;
	}
}
