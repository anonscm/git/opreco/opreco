package de.tarent.oprecoserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class Main {

	/**
	 * Start the key control application.
	 * Parameters: --client/--server
	 * @throws SocketException 
	 */
	public static String homepath; 
	
	
	public static void main(String[] args) {
		
		homepath = System.getProperty("user.home")+"/.opreco";
		File oprecoFolder = new File(homepath);
		if(!oprecoFolder.exists())
			oprecoFolder.mkdir();
		
		if (args[0].equals("--server")) {
			SocketServerTransport.getInstance().setPort(2000);
			ServerTransport transport = SocketServerTransport.getInstance();
			Server server = new Server(transport);
			server.start();
			
		} else if (args[0].equals("--daemon")) {
			ServerTransport transport = null;
			SocketServerTransport.getInstance().setDaemonMode(true);
			if(args.length > 1){
				SocketServerTransport.getInstance().setPort(2000);
				SocketServerTransport.getInstance().setPassword(args[1]);
				transport = SocketServerTransport.getInstance();
			}else{
				SocketServerTransport.getInstance().setPort(2000);
				SocketServerTransport.getInstance().setPassword("1234");
				transport = SocketServerTransport.getInstance();
			}
			Server server = new Server(transport);
			server.start(true);
		} else {
			System.out.println("usage: oprecon --server\n" +
								               "--daemon password\n");
			System.exit(1);
		}
	}

}
