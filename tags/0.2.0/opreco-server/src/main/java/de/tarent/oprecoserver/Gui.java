package de.tarent.oprecoserver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;

public class Gui {
	static Gui gui;
	
	String currentClient = "No client connected";
	public MenuItem subItem1; 
	String username;
	public ToolTip tip;
	public Display display;
	Shell shell;
	
	
	public void showToolTipNewClient(){
		display.asyncExec(new Runnable() {
			public void run() {
				tip.setText("New client");
				tip.setMessage("A new client connected");
				tip.setVisible(true);
			}
		});
	}
	
	public void showToolTipBadPassword(){
		display.asyncExec(new Runnable() {
			public void run() {
				tip.setText("Connection error");
				tip.setMessage("Client tried to connect with bad password");
				tip.setVisible(true);
			}
		});
	}
	
	public void setSubItemText(String text){
		subItem1.setText(text);
	}
	
	public void trayIcon(final boolean askForPassword){
			new Thread(new Runnable() {
				public void run() {
					display = new Display();
					

					
					shell = new Shell(display, SWT.DIALOG_TRIM
							| SWT.APPLICATION_MODAL);
	
					
					shell.addDisposeListener(new DisposeListener() {
					      public void widgetDisposed(DisposeEvent e) {
								SocketServerTransport.getInstance().getSocketHandlingBoolean()[SocketServerTransport.getInstance().socketCount - 1] = false;
								try {
									Runtime.getRuntime().exec("/usr/share/java/opreco/pand-killall");
								} catch (IOException i) {
								}
					      }
					});
							
			
					
					Image image = new Image(display,
							"/usr/share/icons/hicolor/24x24/apps/opreco_icon.png");
					
					tip = new ToolTip(shell, SWT.BALLOON
							| SWT.ICON_INFORMATION);
					final Tray tray = display.getSystemTray();
					if (tray == null) {
						SocketServerTransport.getInstance().addlog("The system tray is not available");
					} else {
						TrayItem item = new TrayItem(tray, SWT.NONE);
						item.setToolTip(tip);


						final Menu menu = new Menu(shell, SWT.POP_UP);

						MenuItem headline = new MenuItem(menu, SWT.PUSH);
						headline.setText("Opreco Server");
						headline.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								Shell dialog = new Shell(shell);
								dialog.setSize(400, 200);
								dialog.setLayout(new FillLayout());

								Label label = new Label(dialog, SWT.NONE);
								label.setAlignment(SWT.CENTER);
								label
										.setText("Java - Opreco Server\n "
												+ "A tool to control your OpenOffice presentations\n "
												+ "on your computer with your Jalimo and Linux powered\n "
												+ "cellphone via Bluetooth or LAN/W-Lan");
								dialog.open();
								while (!dialog.isDisposed()) {
									if (!display.readAndDispatch())
										display.sleep();
								}

							}
						});

						MenuItem seperator = new MenuItem(menu, SWT.SEPARATOR);

						MenuItem stop = new MenuItem(menu, SWT.CASCADE);
						stop.setText("Disconnect client");
						Menu subMenu = new Menu(menu);
						stop.setMenu(subMenu);
						subItem1 = new MenuItem(subMenu, SWT.PUSH);
						subItem1.setText("No client connected");
						subItem1.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								SocketServerTransport.getInstance().getSocketHandlingBoolean()[SocketServerTransport.getInstance().socketCount - 1] = false;
								try {
									Runtime.getRuntime().exec("/usr/share/java/opreco/pand-server");
								} catch (IOException e) {
								}
								subItem1.setText("No client connected");
							}
						});
						
						MenuItem restartServer = new MenuItem(menu, SWT.CASCADE);
						restartServer.setText("Restart server");
						restartServer.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								SocketServerTransport.getInstance().getSocketHandlingBoolean()[SocketServerTransport.getInstance().socketCount - 1] = false;
								try {
									Runtime.getRuntime().exec("/usr/share/java/opreco/pand-server");
								} catch (IOException e) {
								}
							}
						});
						
						MenuItem changePassword = new MenuItem(menu, SWT.CASCADE);
						changePassword.setText("Change password");
						changePassword.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								passwordDialog();
							}
						});

						MenuItem clientHistory = new MenuItem(menu, SWT.PUSH);
						clientHistory.setText("Client history");
						clientHistory.addListener(SWT.Selection,
								new Listener() {
									public void handleEvent(Event event) {

										Shell dialog = new Shell(shell);
										dialog
												.setText("Opreco - Client history");
										dialog.setSize(400, 500);
										dialog.setLayout(new FillLayout());

										List list = new List(dialog,
												SWT.V_SCROLL);
										for (int i = 0; i < SocketServerTransport.getInstance().getClientList().size(); i++) {
											list.add(SocketServerTransport.getInstance().getClientList().get(i));
										}

										dialog.open();
										while (!dialog.isDisposed()) {
											if (!display.readAndDispatch())
												display.sleep();
										}
									}
								});

						MenuItem log = new MenuItem(menu, SWT.PUSH);
						log.setText("View Log");
						log.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {

								Shell dialog = new Shell(shell);
								dialog.setText("Remote Key Control - Log");
								dialog.setSize(400, 500);
								dialog.setLayout(new FillLayout());

								List list = new List(dialog, SWT.V_SCROLL);
								for (int i = 0; i < SocketServerTransport.getInstance().getLogList().size(); i++) {
									list.add(SocketServerTransport.getInstance().getLogList().get(i));
								}

								dialog.open();
								while (!dialog.isDisposed()) {
									if (!display.readAndDispatch())
										display.sleep();
								}
							}
						});

						MenuItem separator2 = new MenuItem(menu, SWT.SEPARATOR);

						MenuItem quit = new MenuItem(menu, SWT.PUSH);
						quit.setText("Quit");
						quit.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								try {
									SocketServerTransport.getInstance().getServerSocket().close();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								shell.dispose();
								System.exit(1);
							}
						});

						if(askForPassword)
							passwordDialog();

						item.addListener(SWT.MenuDetect, new Listener() {
							public void handleEvent(Event event) {
								menu.setVisible(true);
							}
						});
						item.setImage(image);
						
					}
					while (!shell.isDisposed()) {
						if (!display.readAndDispatch())
							display.sleep();
					}

				}
			}).start();
		}
	
	public void passwordDialog(){
		final Shell dialog = new Shell(shell, SWT.DIALOG_TRIM
				| SWT.APPLICATION_MODAL);
		dialog.setText("Enter password for Login");
		dialog.setSize(350, 100);
		GridLayout dialogLayout = new GridLayout(3, false);
		dialogLayout.horizontalSpacing = 20;
		dialogLayout.verticalSpacing = 20;
		dialog.setLayout(dialogLayout);

		final Text password = new Text(dialog, SWT.NONE);

		password.setEchoChar('*');

		GridData passwordButtonGridData = new GridData(
				SWT.RIGHT, SWT.BOTTOM, false, false);
		passwordButtonGridData.widthHint = 170;
		password.setLayoutData(passwordButtonGridData);

		GridData buttonGridData = new GridData(SWT.RIGHT,
				SWT.BOTTOM, false, false);
		buttonGridData.widthHint = 50;

		Button enter = new Button(dialog, SWT.PUSH);
		enter.setText("Enter");
		enter.setLayoutData(buttonGridData);


		Button exit = new Button(dialog, SWT.PUSH);
		exit.setText("Exit");
		exit.setLayoutData(buttonGridData);
		exit.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				System.exit(1);
			}
		});

		/*
		 * if(getPasswordFromWallet() != null){
		 * password.setText(getPasswordFromWallet()); }
		 */

		final Button checkbox = new Button(dialog, SWT.CHECK);
		checkbox.setText("Save password");

		enter.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				if(checkbox.getSelection()){
					try {
						writePasswordToFile(password.getText());
					} catch (IOException e) {
					}
				SocketServerTransport.password = password
						.getText();
				}else{
					disableSavedPassword();
					SocketServerTransport.getInstance().generatePassword(password.getText());
				}
				dialog.dispose();
			}
		});
		
		password.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.keyCode == SWT.CR) {
					SocketServerTransport.password = password
							.getText();
					if(checkbox.getSelection()){
						try {
							writePasswordToFile(password.getText());
						} catch (IOException e1) {
						}
					}else{
						disableSavedPassword();
						SocketServerTransport.getInstance().generatePassword(password.getText());
					}
					dialog.dispose();
				}

			}
		});

		dialog.open();
	}
	
	public void disableSavedPassword(){
		File a = new File(Main.homepath + "/password.settings");
		if(a.exists())
			a.delete();
	}

	public void writePasswordToFile(String passwordtmp) throws IOException{
		File a = new File(Main.homepath + "/password.settings");
		FileWriter fw = new FileWriter(a);
		BufferedWriter bw = new BufferedWriter(fw);
		String randomNumber = String.valueOf(Math.floor(Math.random() * 100) + 1);
		SocketServerTransport.randomNumber = randomNumber;
		SocketServerTransport.md5Password = ChecksumTool.createChecksum(passwordtmp + randomNumber);
		bw.write(randomNumber);
		bw.write("\n" + ChecksumTool.createChecksum(passwordtmp + randomNumber));
		bw.write("\n"+passwordtmp);
		bw.close();
	}
	
	public static Gui getInstance(){
		if(gui == null)
			gui = new Gui();
		return gui;
	}
}
