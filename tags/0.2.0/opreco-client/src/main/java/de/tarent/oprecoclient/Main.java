package de.tarent.oprecoclient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;


public class Main {

	/**
	 * Start the key control application.
	 * Parameters: --client/--server
	 * @throws SocketException 
	 */
	public static String homepath;
	public static String distribution;
	
	
	public static void main(String[] args) {
		
		File oprecoFolder = new File(System.getProperty("user.home")+"/.opreco");
		homepath = oprecoFolder.getAbsolutePath();
		System.out.println("Homepath: "+homepath);
		if(!oprecoFolder.exists())
			oprecoFolder.mkdir();
		
		try {
			BufferedReader distriReader = new BufferedReader(new FileReader(new File("/usr/share/java/opreco/distribution.config")));
			distribution = distriReader.readLine();
			System.out.println("Distri: "+distribution);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String host = "127.0.0.1";
		int port = 2000;
		if (args.length == 3) {
			host = args[1];
			port = Integer.parseInt(args[2]);
		} else {
			// choose a usable default by showing the own ip address
			if (!"Windows CE".equals(System.getProperty("os.name"))) {
				try {
					NetworkInterface ni = NetworkInterface.getByName("eth0");
					if (ni == null)
						ni = NetworkInterface.getByName("eth1");
					if (ni != null) {
						Enumeration<InetAddress> addresses = ni.getInetAddresses();
						while (addresses.hasMoreElements()) {
							InetAddress address = addresses.nextElement();
							if (address instanceof Inet4Address)
								host = address.getHostAddress();
						}
					}
				} catch (SocketException e) {
					// 	do nothing
				}
			}
				
		}
		ClientTransport transport = SocketClientTransport.getInstance();
		transport.setPort(port);
		transport.setHost(host);
		ControlClient client = ControlClient.newControlClient(transport);
		client.start();
		
	}

}
