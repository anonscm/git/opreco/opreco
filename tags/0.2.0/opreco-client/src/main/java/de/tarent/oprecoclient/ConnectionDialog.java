package de.tarent.oprecoclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;


public class ConnectionDialog {

	Shell dialog;

	ClientTransport selectedTransport;

	SocketClientTransport socketTransport;

	MotionControl motionControl;

	Text hostEntry;

	Text portEntry;

	Text passwordEntry;

	Text btPasswordEntry;

	List list;
	
	Button autoConnectCheck;

	static String password;

	String btHost = "";

	boolean motionStatus = false;

	boolean statusOk;

	boolean refresh_pressed = false;

	public void motionControlStart() {
		if ((!MotionControl.status)) {
			motionControl = new MotionControl(socketTransport);
			MotionControl.status = true;
		}
	}

	Listener connectListener = new Listener() {
		public void handleEvent(Event arg0) {
			try {
				socketTransport = (SocketClientTransport) selectedTransport;
				socketTransport.setHost(hostEntry.getText());
				socketTransport.setPort(Integer.parseInt(portEntry.getText()));
				socketTransport.setPassword(passwordEntry.getText());
				if(Main.distribution.equals("openmoko")){
					motionControlStart();
					motionControl.start();
				}
				statusOk = true;
				dialog.dispose();
				
			} catch (NumberFormatException nfe) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("wrong value");
				mb.setMessage("The port should be a number!");
				mb.open();
			}
		}
	};

	Listener bluetoothConnectButtonListener = new Listener() {
		public void handleEvent(Event arg0) {
			bluetoothConnect();
		}
	};

	Listener cancelListener = new Listener() {
		public void handleEvent(Event arg0) {
			statusOk = false;
			dialog.dispose();
		}
	};

	public void initConnectionDialog(Shell parent) {

		dialog = new Shell();// , SWT.DIALOG_TRIM |
								  // SWT.APPLICATION_MODAL);
		dialog.setText("Connection");
		GridLayout dialogLayout = new GridLayout(2, false);
		//dialog.setSize(ControlClient.getDisplay().getBounds().width, ControlClient.getDisplay().getBounds().height);
		dialog.setLayout(dialogLayout);

		final TabFolder tabFolder = new TabFolder(dialog, SWT.NONE);
		TabItem bluetooth = new TabItem(tabFolder, SWT.NONE);
		bluetooth.setText("Bluetooth");
		TabItem lan = new TabItem(tabFolder, SWT.NONE);
		lan.setText("LAN/W-LAN");
		bluetooth.addListener(SWT.Selection, bluetoothListener);

		GridData gridDataTabFolder = new GridData(SWT.FILL, SWT.FILL, true,
				true, 2, 4);
		tabFolder.setLayoutData(gridDataTabFolder);

		Button cancelGeneralButton = new Button(dialog, SWT.PUSH);
		cancelGeneralButton.setText("Cancel");
		GridData cancelGeneralButtonGridData = new GridData(SWT.FILL, SWT.FILL,
				true, false, 2, 1);
		cancelGeneralButton.setLayoutData(cancelGeneralButtonGridData);
		cancelGeneralButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				dialog.setVisible(false);
			}
		});

		// socketConfiguration
		Composite socketConfiguration = new Composite(tabFolder, SWT.NONE);
		socketConfiguration.setLayout(new GridLayout(2, false));
		socketConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
				true, 2, 1));

		hostEntry = appendFormEntry(socketConfiguration, "Host");
		portEntry = appendFormEntry(socketConfiguration, "Port");
		passwordEntry = appendFormEntry(socketConfiguration, "Password");

		// button bar
		Button cancelButton = new Button(socketConfiguration, SWT.NONE);
		GridData cancelGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false,
				false);
		cancelGridData.widthHint = 100;
		cancelGridData.heightHint = 100;
		cancelButton.setLayoutData(cancelGridData);
		cancelButton.setText("Cancel");
		cancelButton.addListener(SWT.Selection, cancelListener);

		Button connectButton = new Button(socketConfiguration, SWT.NONE);
		GridData connectGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false,
				false);
		connectGridData.widthHint = 100;
		connectGridData.heightHint = 100;
		connectButton.setLayoutData(connectGridData);
		connectButton.setText("Connect");
		connectButton.addListener(SWT.Selection, connectListener);

		lan.setControl(socketConfiguration);

		// BluetoothConfiguration

		Composite bluetoothConfiguration = new Composite(tabFolder, SWT.NONE);
		bluetoothConfiguration.setLayout(new GridLayout(4, false));
		bluetoothConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.TOP,
				true, true, 2, 1));

		Button bluetoothConnectButton = new Button(bluetoothConfiguration,
				SWT.PUSH);
		GridData ButtonGridData = new GridData(SWT.FILL, SWT.BOTTOM, true,
				false, 2, 2);
		ButtonGridData.heightHint = (int) (Display.getCurrent().getBounds().height / 8);
		bluetoothConnectButton.setLayoutData(ButtonGridData);

		bluetoothConnectButton.setText("Connect");
		bluetoothConnectButton.addListener(SWT.Selection,
				bluetoothConnectButtonListener);

		Button refreshButton = new Button(bluetoothConfiguration, SWT.PUSH);
		refreshButton.setLayoutData(ButtonGridData);
		refreshButton.setText("Scan");
		refreshButton.addListener(SWT.Selection, bluetoothListener);

		Label passwordLabel = new Label(bluetoothConfiguration, SWT.NONE);
		GridData lbPasswordEntryGridData = new GridData(SWT.FILL, SWT.FILL,
				false, false, 1, 1);
		passwordLabel.setLayoutData(lbPasswordEntryGridData);
		passwordLabel.setText("Password:");

		btPasswordEntry = new Text(bluetoothConfiguration, SWT.BORDER
				| SWT.MULTI);
		GridData btPasswordEntryGridData = new GridData(SWT.FILL, SWT.FILL,
				false, false, 3, 1);
		btPasswordEntry.setLayoutData(btPasswordEntryGridData);
		btPasswordEntry.setText("1234");
		
		autoConnectCheck = new Button(bluetoothConfiguration, SWT.CHECK);
		autoConnectCheck.setText("Autoconnect next time?");
		GridData autoConnectCheckGridData = new GridData(SWT.FILL, SWT.FILL,
				false, false, 4, 1);
		autoConnectCheck.setLayoutData(autoConnectCheckGridData);

		list = new List(bluetoothConfiguration, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL);
		GridData bluetoothConfigurationGridData = new GridData(SWT.FILL,
				SWT.FILL, true, true, 4, 5);
		list.setLayoutData(bluetoothConfigurationGridData);

		bluetooth.setControl(bluetoothConfiguration);

		clearListAsync();
		String[] defaults = getBTDefaults();
		if (defaults[0] != null) {
			setNewContentAsync("last connection");
		}
		// dialog.pack();
		open();

	}

	public ConnectionDialog(Shell parent, ClientTransport preSelectedTransport,
			boolean allowAutoConnect) {
		selectedTransport = preSelectedTransport;
		if (allowAutoConnect) {
			String[] defaults = getBTDefaults();
			if ((defaults[2] != null)
					&& (allowAutoConnect && defaults[2].equals("autoconnect"))) {
				// list.add("last connection"); // fix
				if (!bluetoothConnect()) {
					initConnectionDialog(parent);
				}
			} else {
				initConnectionDialog(parent);
			}
		} else
			initConnectionDialog(parent);
	}

	Process proc = null;

	private void btsearch() {
		new Thread(new Runnable() {
			public void run() {
				String first = "";
				System.out.println("Searching for Bluetooth devices");
				try {
					clearListAsync();
					setNewContentAsync("Searching...");
					proc = Runtime.getRuntime().exec("hcitool scan");
				} catch (IOException e) {
					e.printStackTrace();
				}
				BufferedReader in = new BufferedReader(new InputStreamReader(
						proc.getInputStream()));
				String[] test = new String[100];
				int i = 0;
				try {
					first = in.readLine();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				System.out.println(first);
				System.out.println("Search finished");
				while (true) {
					try {
						test[i] = in.readLine();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (test[i] == null) {
						if (i == 0) {
							setNewContentAsync("Nothing found");
						}
						break;
					}
					i++;
				}
				clearListAsync();
				for (int e = 0; e < i; e++) {
					if (test[e] != null) {
						setNewContentAsync(test[e]);
						System.out.println(test[e]);
					}
				}
				if (first == null) {
					setNewContentAsync("No powered bluetooth");
					setNewContentAsync("device found.");
				}
			}
		}).start();
	}

	public void setNewContent(String string) {
		list.add(string);
	}

	public void clearListAsync() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				list.removeAll();
			}
		});
	}

	public void setNewContentAsync(final String string) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				setNewContent(string);
			}
		});
	}

	Listener bluetoothListener = new Listener() {
		public void handleEvent(Event arg0) {
			System.out.println("BluetoothListener");
			refresh_pressed = true;
			btsearch();
		}
	};

	public boolean isStatusOk() {
		return statusOk;
	}

	/**
	 * @param socketConfiguration
	 */
	private Text appendFormEntry(Composite socketConfiguration, String label) {
		Label labelComponent = new Label(socketConfiguration, SWT.NONE);
		labelComponent.setText(label);
		Text entry = new Text(socketConfiguration, SWT.SINGLE);
		GridData entryLayout = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
		entryLayout.widthHint = 300;
		entry.setLayoutData(entryLayout);
		return entry;
	}

	/**
	 * 
	 * @param currentTransport
	 *            the transport currently used, may be null
	 */
	private void open() {
		socketTransport = (SocketClientTransport) selectedTransport;
		hostEntry.setText(saveNull(socketTransport.getHost()));
		portEntry.setText("" + socketTransport.getPort());
		passwordEntry.setText(saveNull(socketTransport.getPassword()));

		statusOk = false;
		dialog.open();
		dialog.setFullScreen(true);
		Display display;
		if (Display.getCurrent() == null)
			display = new Display();
		else
			display = Display.getCurrent();
		while (!dialog.isDisposed())
			display.readAndDispatch();
	}

	private String saveNull(String string) {
		return string != null ? string : "";
	}

	public ClientTransport getSelectedTransport() {
		return selectedTransport;
	}

	private String[] getBTDefaults() {
		try {
			File a = new File(Main.homepath + "/connection.settings");
			FileReader fr = new FileReader(a);
			BufferedReader br = new BufferedReader(fr);

			String mac = br.readLine();
			String pw = br.readLine();
			String autoconnect = br.readLine();
			br.close();
			String[] defaults = new String[3];
			defaults[0] = mac;
			defaults[1] = pw;
			defaults[2] = autoconnect;
			return defaults;

		} catch (IOException e) {
			System.err.println("Could not read connection settings from file.");
		}
		String[] error = new String[3];
		error[0] = null;
		error[1] = null;
		error[2] = null;
		return error;
	}

	boolean bluetoothConnect() {
		String[] test = null;
		try {
			test = list.getSelection();
		} catch (Exception e) {
		}

		if ((refresh_pressed) && (test.length == 0)) {
			MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
			mb.setText("Error");
			mb.setMessage("Please scan for devices and choose");
			mb.open();
			return false;
		} else {

			String mac = "";
			String pw = "";
			String autoconnect = "";
			if (!refresh_pressed) {
				String[] defaults = getBTDefaults();
				mac = defaults[0];
				pw = defaults[1];
				autoconnect = defaults[2];
			} else {
				pw = btPasswordEntry.getText();
				for (int i = 1; i < 18; i++) {
					mac = mac + test[0].charAt(i);
				}
			}
			try {
				Runtime.getRuntime().exec("/usr/share/java/opreco/pand-killall");
				Runtime.getRuntime().exec("/usr/share/java/opreco/pand-connect "+mac);
/*				Runtime.getRuntime().exec("pand --killall");
				Runtime.getRuntime().exec(
						"pand --connect " + mac + " --autozap"); */
				System.out.println("pand connected!");
			} catch (IOException e) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("Error");
				mb.setMessage("Error PAN setup");
				mb.open();
				e.printStackTrace();
			}
			Process ifconfig = null;
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			try {
				/*Process ifconfig2 = Runtime.getRuntime().exec(
						"ifconfig bnep0 172.16.0.2");*/

				Runtime.getRuntime().exec("/usr/share/java/opreco/pand-ifconfig 172.16.0.2");
				
				Thread.sleep(1000);

				Process locateHostIp = Runtime.getRuntime().exec(
						"ping -c 1 172.16.0.1");
				BufferedReader locateHostIpInputReader = new BufferedReader(
						new InputStreamReader(locateHostIp.getInputStream()));

				String tmp = locateHostIpInputReader.readLine()
						+ locateHostIpInputReader.readLine()
						+ locateHostIpInputReader.readLine()
						+ locateHostIpInputReader.readLine();
				System.out.println(tmp);

				if (!tmp.contains("100% packet loss") && !tmp.contains("Destination Host Unreachable")) {
					btHost = "172.16.0.1";
				} else {
					Runtime.getRuntime().exec("/usr/share/java/opreco/pand-ifconfig 10.0.0.2");
					locateHostIp = Runtime.getRuntime().exec(
							"ping -s bnep0 -c 1 10.0.0.1");
					locateHostIpInputReader = new BufferedReader(
							new InputStreamReader(locateHostIp.getInputStream()));
					tmp = locateHostIpInputReader.readLine()
					+ locateHostIpInputReader.readLine()
					+ locateHostIpInputReader.readLine()
					+ locateHostIpInputReader.readLine();
						System.out.println(tmp);
					if (!tmp.contains(
							"100% packet loss")) {
						btHost = "10.0.0.1";
					}
				}

				if (btHost.equals("")) {
					MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
					mb.setText("Error");
					mb
							.setMessage("Remote host isn't running the Opreco-Service");
					mb.open();
				}
			} catch (IOException e) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("Error");
				mb.setMessage("Error during interface setup");
				mb.open();
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (!btHost.equals("")) {
				try {
					socketTransport = (SocketClientTransport) selectedTransport;
					socketTransport.setHost(btHost);
					socketTransport.setPort(2000);
					socketTransport.setPassword(pw);

					if(Main.distribution.equals("openmoko")){
						motionControlStart();
						MotionControl.start();
					}

					try {
						File a = new File(Main.homepath + "/connection.settings");
						FileWriter fw = new FileWriter(a);
						BufferedWriter bw = new BufferedWriter(fw);
						bw.write(mac);
						bw.write("\n" + pw);
						if (autoConnectCheck !=null && autoConnectCheck.getSelection()){
							ControlClient.getInstance().saveConn.setSelection(true);
							bw.write("\nautoconnect");
						}else{
							bw.write("\nno autoconnect");
							ControlClient.getInstance().saveConn.setSelection(false);
						}
						bw.close();
						
					} catch (IOException e) {
						System.err
								.println("Could not write connection settings to file.");
					}

					statusOk = true;
					if (dialog != null)
						dialog.dispose();
					return true;
				} catch (NumberFormatException nfe) {
					MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
					mb.setText("wrong value");
					mb.setMessage("The port should be a number!");
					mb.open();
				}
			}
			return false;
		}

	}
	


}
