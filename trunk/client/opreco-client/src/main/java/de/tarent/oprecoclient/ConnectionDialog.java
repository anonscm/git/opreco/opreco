package de.tarent.oprecoclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

public class ConnectionDialog implements DiscoveryListener {

	Shell dialog;

	ClientTransport selectedTransport;

	SocketClientTransport socketTransport;

	MotionControl motionControl;

	Text btPasswordEntry;

	List list;

	Button autoConnectCheck;

	static String password;
	
	ArrayList <RemoteDevice> deviceList = new ArrayList<RemoteDevice>();

	String btHost = "";

	boolean motionStatus = false;
	
	boolean connectionDialogRuns = false;

	boolean statusOk = false;

	boolean refresh_pressed = false;
	
	boolean serviceDiscovered = false;

	/*-
	 * 
	 *  ---- Debug attributes ----
	 */

	static final boolean DEBUG = true;

	static final String DEBUG_address = "002243AAD6C6"; // toti-eee

	/*-
	 * 
	 *  ---- Bluetooth attributes ----
	 */
	protected UUID uuid = new UUID(0x1101); // serial port profile

	protected int inquiryMode = DiscoveryAgent.GIAC;

	protected int connectionOptions = ServiceRecord.NOAUTHENTICATE_NOENCRYPT;

	public void motionControlStart() {
		if ((!MotionControl.status)) {
			motionControl = new MotionControl(socketTransport);
			MotionControl.status = true;
		}
	}


	/* Listener fuer Bluetooth verbindung */
	Listener bluetoothConnectButtonListener = new Listener() {
		public void handleEvent(Event arg0) {
			try {
				SocketClientTransport.getInstance().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			bluetoothConnect();
		}
	};

	public boolean initConnectionDialog() {

		if(connectionDialogRuns)
			return false;
		
		connectionDialogRuns = true;
		dialog = new Shell();
		dialog.setText("Connection");
		GridLayout dialogLayout = new GridLayout(2, false);
		dialog.setLayout(dialogLayout);

		final TabFolder tabFolder = new TabFolder(dialog, SWT.NONE);
		TabItem bluetooth = new TabItem(tabFolder, SWT.NONE);
		bluetooth.setText("Bluetooth Connection");
		bluetooth.addListener(SWT.Selection, bluetoothListener);

		GridData gridDataTabFolder = new GridData(SWT.FILL, SWT.FILL, true,
				true, 2, 4);
		tabFolder.setLayoutData(gridDataTabFolder);

		Button cancelGeneralButton = new Button(dialog, SWT.PUSH);
		cancelGeneralButton.setText("Cancel");
		GridData cancelGeneralButtonGridData = new GridData(SWT.FILL, SWT.FILL,
				true, false, 2, 1);
		cancelGeneralButton.setLayoutData(cancelGeneralButtonGridData);
		cancelGeneralButton.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event event) {
				statusOk = false;
				ControlClient.getInstance().connectionDialogRuns = false;
				dialog.setVisible(false);
			}
		});

		

	
		// BluetoothConfiguration

		Composite bluetoothConfiguration = new Composite(tabFolder, SWT.NONE);
		bluetoothConfiguration.setLayout(new GridLayout(4, false));
		bluetoothConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.TOP,
				true, true, 2, 1));

		Button bluetoothConnectButton = new Button(bluetoothConfiguration,
				SWT.PUSH);
		GridData ButtonGridData = new GridData(SWT.FILL, SWT.BOTTOM, true,
				false, 2, 2);
		ButtonGridData.heightHint = (int) (Display.getCurrent().getBounds().height / 8);
		bluetoothConnectButton.setLayoutData(ButtonGridData);

		bluetoothConnectButton.setText("Connect");
		bluetoothConnectButton.addListener(SWT.Selection,
				bluetoothConnectButtonListener);

		Button refreshButton = new Button(bluetoothConfiguration, SWT.PUSH);
		refreshButton.setLayoutData(ButtonGridData);
		refreshButton.setText("Scan");
		refreshButton.addListener(SWT.Selection, bluetoothListener);

		Label passwordLabel = new Label(bluetoothConfiguration, SWT.NONE);
		GridData lbPasswordEntryGridData = new GridData(SWT.FILL, SWT.FILL,
				false, false, 1, 1);
		passwordLabel.setLayoutData(lbPasswordEntryGridData);
		passwordLabel.setText("Password:");

		btPasswordEntry = new Text(bluetoothConfiguration, SWT.BORDER
				| SWT.MULTI);
		GridData btPasswordEntryGridData = new GridData(SWT.FILL, SWT.FILL,
				false, false, 3, 1);
		btPasswordEntry.setLayoutData(btPasswordEntryGridData);
		btPasswordEntry.setText("1234");

		autoConnectCheck = new Button(bluetoothConfiguration, SWT.CHECK);
		autoConnectCheck.setText("Autoconnect next time?");
		GridData autoConnectCheckGridData = new GridData(SWT.FILL, SWT.FILL,
				false, false, 4, 1);
		autoConnectCheck.setLayoutData(autoConnectCheckGridData);

		list = new List(bluetoothConfiguration, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL);
		GridData bluetoothConfigurationGridData = new GridData(SWT.FILL,
				SWT.FILL, true, true, 4, 5);
		list.setLayoutData(bluetoothConfigurationGridData);

		bluetooth.setControl(bluetoothConfiguration);

		clearListAsync();
		String[] defaults = getBTDefaults();
		if (defaults[0] != null) {
			setNewContentAsync("last connection");
		}
		open();
		
		return true;

	}

	public ConnectionDialog(ClientTransport preSelectedTransport,
			boolean allowAutoConnect) {
	
		selectedTransport = preSelectedTransport;
		if (allowAutoConnect) {
			String[] defaults = getBTDefaults();
			if ((defaults[2] != null)
					&& (allowAutoConnect && defaults[2].equals("autoconnect"))) {
				socketTransport = (SocketClientTransport) selectedTransport;
				statusOk = false;
				if (!bluetoothConnect()) {
					initConnectionDialog();
				}
			} else {
				initConnectionDialog();
			}
		} else
			initConnectionDialog();
	}

	Process proc = null;

	private void btsearch() {
		clearListAsync();
		startDeviceInquiry();
	}


	public void setNewContent(String string) {
		if(!list.isDisposed())
			list.add(string);
	}

	public void clearListAsync() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				list.removeAll();
			}
		});
	}

	public void setNewContentAsync(final String string) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				setNewContent(string);
			}
		});
	}

	Listener bluetoothListener = new Listener() {
		public void handleEvent(Event arg0) {
			System.out.println("BluetoothListener");
			refresh_pressed = true;
			btsearch();
		}
	};

	public boolean isStatusOk() {
		return statusOk;
	}



	/**
	 * 
	 * @param currentTransport
	 *            the transport currently used, may be null
	 */
	private void open() {
		socketTransport = (SocketClientTransport) selectedTransport;
		
		statusOk = false;
		dialog.open();
		dialog.setFullScreen(true);
		Display display;
		if (Display.getCurrent() == null)
			display = new Display();
		else
			display = Display.getCurrent();
		while (!dialog.isDisposed())
			display.readAndDispatch();
	}

	
	public ClientTransport getSelectedTransport() {
		return selectedTransport;
	}

	private String[] getBTDefaults() {
		try {
			File a = new File(Main.homepath + "/connection.settings");
			FileReader fr = new FileReader(a);
			BufferedReader br = new BufferedReader(fr);

			String mac = br.readLine();
			String pw = br.readLine();
			String autoconnect = br.readLine();
			br.close();
			String[] defaults = new String[3];
			defaults[0] = mac;
			defaults[1] = pw;
			defaults[2] = autoconnect;
			return defaults;

		} catch (IOException e) {
			System.err.println("Could not read connection settings from file.");
		}
		String[] error = new String[3];
		error[0] = null;
		error[1] = null;
		error[2] = null;
		return error;
	}

	boolean bluetoothConnect() {

		if ((refresh_pressed) && (list.getSelection().length == 0)) {
			MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
			mb.setText("Error");
			mb.setMessage("Please scan for devices and choose");
			mb.open();
			return false;
		} else {

			String mac = "";
			String pw = "";
			if (!refresh_pressed) {
				String[] defaults = getBTDefaults();
				mac = defaults[0];
				pw = defaults[1];
//				autoconnect = defaults[2];
			} else {
				pw = btPasswordEntry.getText();
				mac = deviceList.get(list.getSelectionIndex()).getBluetoothAddress();
			}
			try {
				if(socketTransport == null)
					System.out.println("socketTransport == null");
			
				socketTransport.setPassword(pw);
				startServiceSearch(new RemoteDevice(mac){});
			} catch (Exception e) {
				e.printStackTrace();
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("Error");
				mb.setMessage("Error on connecting");
				mb.open();
				e.printStackTrace();
			}

			if (Main.distribution.equals("openmoko")) {
				motionControlStart();
				MotionControl.start();
			}

			writeConnectionSettings(mac, pw);
			
			
			if (dialog != null){
				dialog.dispose();
				connectionDialogRuns = false;
			}
			return true;
		}

	}

	public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
		System.out.println("A device discovered (" + getDeviceStr(btDevice)
				+ ")");
		if(deviceList.size() == 0) {
			clearListAsync();
		}
		setNewContentAsync(getDeviceStr(btDevice));
		deviceList.add(btDevice);
	}

	public void servicesDiscovered(int transId, ServiceRecord[] records) {
		System.out.println("Service discovered.");
		for (int i = 0; i < records.length; i++) {
			System.out.println("records: " + records.length);
			ServiceRecord rec = records[i];
			String url = rec.getConnectionURL(connectionOptions, false);
			socketTransport.url = url;
			serviceDiscovered = true;
			statusOk = true;
		}
	}


	private void startServiceSearch(RemoteDevice device) {
		try {
			System.out.println("Start search for Serial Port Profile service from "+ getDeviceStr(device));
			UUID uuids[] = new UUID[] { uuid };
			serviceDiscovered = false;
			getAgent().searchServices(null, uuids, device, this);
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	private String getDeviceStr(RemoteDevice btDevice) {
		return getFriendlyName(btDevice) + " - 0x"
				+ btDevice.getBluetoothAddress();
	}

	private String getFriendlyName(RemoteDevice btDevice) {
		try {
			return btDevice.getFriendlyName(false);
		} catch (IOException e) {
			return "no name available";
		}
	}

	private DiscoveryAgent getAgent() {
		try {
			return LocalDevice.getLocalDevice().getDiscoveryAgent();
		} catch (BluetoothStateException e) {
			System.err.println(e);
			System.err.println("ERROR detected and all operations stopped.");
			throw new Error("No discovery agent available.");
		}
	}

	private void startDeviceInquiry() {
		try {
			System.out
					.println("Scanning for Devices...");
			deviceList.clear();
			clearListAsync();
			setNewContentAsync("Scanning...");
			getAgent().startInquiry(inquiryMode, this);
		} catch (Exception e) {
			System.err.println(e);
		}
	}

	public void serviceSearchCompleted(int transID, int respCode) {
		String msg = null;
		switch (respCode) {
		case SERVICE_SEARCH_COMPLETED:
			msg = "the service search completed normally";
			break;
		case SERVICE_SEARCH_TERMINATED:
			msg = "the service search request was cancelled by a call to DiscoveryAgent.cancelServiceSearch()";
			break;
		case SERVICE_SEARCH_ERROR:
			msg = "an error occurred while processing the request";
			break;
		case SERVICE_SEARCH_NO_RECORDS:
			msg = "no records were found during the service search";
			break;
		case SERVICE_SEARCH_DEVICE_NOT_REACHABLE:
			msg = "the device specified in the search request could not be reached or the local device could not establish a connection to the remote device";
			break;
		}
		System.out.println("Service search completed - " + msg);
		if(!serviceDiscovered) {
			System.out.println("no service found!");
//			ControlClient.getInstance().publishErrorAsync("Error", "Opreco Server not running!");
		}
		
		final String tmp = msg;	
	
		
		if(!msg.equals("the service search completed normally") || (!serviceDiscovered)){
			Display.getDefault().asyncExec(new Runnable() {
				public void run(){
					initConnectionDialog();
					
					
				}
			});
		}
		connectionDialogRuns = false;
		
			
	}

	public void inquiryCompleted(int discType) {
		System.out.println("Search finished");
		if(!this.dialog.isDisposed())
			setNewContentAsync("Search done.");
	}
	
	public void writeConnectionSettings(String mac, String pw) {
		try {
			File a = new File(Main.homepath + "/connection.settings");
			FileWriter fw = new FileWriter(a);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(mac);
			bw.write("\n" + pw);
			if (autoConnectCheck != null && !autoConnectCheck.getSelection()) {
				bw.write("\nno autoconnect");
				ControlClient.getInstance().saveConn.setSelection(false);
			} else {
				ControlClient.getInstance().saveConn.setSelection(true);
				bw.write("\nautoconnect");
			}
			bw.close();

		} catch (IOException e) {
			System.err
					.println("Could not write connection settings to file.");
		}
	}

}
