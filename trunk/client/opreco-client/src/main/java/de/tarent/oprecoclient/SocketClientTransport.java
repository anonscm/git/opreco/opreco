package de.tarent.oprecoclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import javax.bluetooth.LocalDevice;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

import org.eclipse.swt.widgets.Display;

public class SocketClientTransport implements ClientTransport {

	int port;
	String host;
	String password;
	Socket socket;
	OutputStreamWriter writer;
	BufferedReader reader;
	static SocketClientTransport socketClientTransport;
	String url = null;
	StreamConnection stream = null;
	/**
	 * Create a new Socket connection to the supplied port
	 * @param port
	 */
	
	public void close() throws IOException {
		
		if(!(stream==null)){
			stream.close();
		}
	}

	public boolean connect() throws IOException {
			try {
				System.out.println("Connecting to server by url: " + url);
				stream = (StreamConnection) Connector.open(url);
				

				System.out.println("Bluetooth stream open.");
				reader = new BufferedReader(new InputStreamReader(stream.openInputStream()));
				writer = new OutputStreamWriter(stream.openOutputStream());	
			} catch (IOException e) {
				ControlClient.getInstance().openConnectionDialog(false);
				System.err.println(e);
			}
		
		System.out.println("Send: "+password);
		
		String random = reader.readLine();
		System.out.println(random);
		System.out.println(ChecksumTool.createChecksum(password+random));
		
		writer.write(ChecksumTool.createChecksum(password+random));
		writer.write("\n");
		writer.flush();
		
		
		writer.write(LocalDevice.getLocalDevice().getFriendlyName());
		writer.write("\n");
		writer.flush();
		
		if(reader.readLine().equals("Bad Password")){
			close();
			return false;
		}
		else {
			return true;
		}
	}
	

	public void sendCommand(String cmd){
		if(cmd.contains(";") ) {
			for (String s : cmd.split(";"))
				sendCommand(s);
			return;
		}
			System.out.println("Sending Command: " + cmd);
			if(writer == null) {
				System.err.println("writer == null");
				Display.getDefault().asyncExec(new Runnable() {
					public void run(){
						ControlClient.getInstance().openConnectionDialog(false);
					}
				});
			}
			try{
				writer.write(cmd);
				writer.write("\n");
				writer.flush();
				String result = reader.readLine();
				handleResult(result);
			}catch(IOException e){
				System.err.println(e);
				Display.getDefault().asyncExec(new Runnable() {
					public void run(){
						ControlClient.getInstance().openConnectionDialog(false);
					}
				});
				
			}
	}
	
	public void handleResult(String result) throws IOException {
		if("kicked, new client".equals(result)){
			stream.close();
			writer.close();
			reader.close();
			throw new IOException("Got Kicked, new client connected");	
		} 
		if("disconnected".equals(result)){
			stream.close();
			writer.close();
			reader.close();
			throw new IOException("Got Kicked, new client connected");	
		}
		else if (result == null)
			throw new IOException("no response from server");
	}
	
	public boolean getBtStatus() throws IOException{
		return true;
	}

	public boolean isConnected() {
		return true;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static SocketClientTransport getInstance(){
		if(socketClientTransport == null){
			socketClientTransport = new SocketClientTransport();
		}
		return socketClientTransport;
	}

}
