package de.tarent.oprecoclient;

import java.io.IOException;

public interface ClientTransport extends Transport {
	
	/**
	 * Connect to the server
	 */
	public boolean connect() throws IOException;

	/**
	 * Send one key command
	 */
	public void sendCommand(String cmd) throws IOException;
	
	
}
