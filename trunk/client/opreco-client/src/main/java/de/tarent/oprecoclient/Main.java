/*
 * opreco,
 * open presentation control
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'opreco'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.oprecoclient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Main {

	/**
	 * Start the key control application.
	 * @throws SocketException 
	 */
	public static String homepath;
	public static String distribution;
	
	
	public static void main(String[] args) {
		
		File oprecoFolder = new File(System.getProperty("user.home")+"/.opreco");
		homepath = oprecoFolder.getAbsolutePath();
		System.out.println("Homepath: "+homepath);
		if(!oprecoFolder.exists())
			oprecoFolder.mkdir();
		
		try {
			BufferedReader distriReader = new BufferedReader(new FileReader(new File("/usr/share/java/opreco/distribution.config")));
			distribution = distriReader.readLine();
			System.out.println("Distri: "+distribution);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	
		ClientTransport transport = SocketClientTransport.getInstance();

		ControlClient client = ControlClient.newControlClient(transport);
		client.start();
	}

}
