package de.tarent.oprecoserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;

public class SocketServerTransport implements ServerTransport {

	// serial port profile
	protected String UUID = new UUID("1101", true).toString();
	
	static SocketServerTransport socketServerTransport;

	
	ServerSocket serverSocket;

	Socket currentSocket;

	boolean sockethandlingboolean[] = new boolean[100];

	boolean connection = true;
	
	boolean bluetooth = false;

	int socketCount = 1;

	boolean daemonMode = false;

	static String password = "";

	static String randomNumber = "";

	static String md5Password = "";
	
	static String remoteName = "";

	String currentClient = "No client connected";

	ArrayList<String> logList = new ArrayList<String>();

	ArrayList<String> clientList = new ArrayList<String>();

	BufferedReader reader;

	OutputStreamWriter writer;
	
	CommandHandler handler;

	public void addlog(String message) {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss ");
		Date currentTime = new Date();
		logList.add(formatter.format(currentTime) + ": " + message);
		System.out.println(formatter.format(currentTime) + ": " + message);
	}

	public void addClientList(String message) {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss ");
		Date currentTime = new Date();
		clientList.add(formatter.format(currentTime) + ": " + message);
	}

	private void btlisten() {
		new Thread(new Runnable() {
			public void run() {
				try {
					LocalDevice device = LocalDevice.getLocalDevice();
					device.setDiscoverable(DiscoveryAgent.GIAC);

					String url = "btspp://localhost:" + UUID
							+ ";name=OprecoServer";

					System.out.println("Create server by uri: " + url);
					StreamConnectionNotifier notifier = (StreamConnectionNotifier) Connector
							.open(url);

					handleConnection(notifier);

				} catch (Throwable e) {
					System.err.println(e);
				}
			}
		}).start();
	}
	
	private void handleConnection(StreamConnectionNotifier notifier) throws IOException {
		while (connection) {
			
			System.out.println("Waiting for connection...");
			StreamConnection conn = notifier.acceptAndOpen();
			System.out.println("Opening connection...");
			
			writer = new OutputStreamWriter(conn.openOutputStream());
			reader = new BufferedReader(new InputStreamReader(conn.openInputStream()));
			bluetooth = true;
			
			writer.write(randomNumber);
			writer.write("\n");
			writer.flush();

			
			// String md5 = ChecksumTool.createChecksum(password + random);
			System.out.println(randomNumber);
			System.out.println(md5Password);
			
			String incommingMD5 = null;
			incommingMD5 = reader.readLine();
			remoteName = reader.readLine();
			if( (incommingMD5 != null) && (incommingMD5.equals(md5Password)) ) {
				new SocketHandling(handler, writer, reader , ++socketCount);
//				socketCount++;
				if (!daemonMode)
					Gui.getInstance().showToolTipNewClient(remoteName);
			} else {
				writer.write("Bad Password");
				writer.write("\n");
				writer.flush();
				
				conn.close();
				writer.close();
				reader.close();
				if (!daemonMode)
					Gui.getInstance().showToolTipBadPassword(remoteName);
			}
		}
	}

	public boolean readPasswordFromFile() throws IOException {
		File passwordFile = new File(Main.homepath + "/password.settings");
		if (!passwordFile.exists())
			return false;
		BufferedReader passwordReader = new BufferedReader(new FileReader(
				passwordFile));
		randomNumber = passwordReader.readLine();
		md5Password = passwordReader.readLine();
		return true;

	}

	public void generatePassword(String passwordtmp) {
		randomNumber = String.valueOf(Math.floor(Math.random() * 100) + 1);
		md5Password = ChecksumTool.createChecksum(passwordtmp + randomNumber);
	}

	public synchronized void listen(CommandHandler handler) throws IOException {
		this.handler = handler;
		addlog("opening server");
		boolean askForPassword = false;

		if (!readPasswordFromFile() && !daemonMode)
			askForPassword = true;
		if (!daemonMode)
			Gui.getInstance().trayIcon(askForPassword);
		if (askForPassword)
			generatePassword(password);

		btlisten();
	}

	public void close() throws IOException {
		writer.close();
		reader.close();
	}
	
	public void kickClient(){
		sockethandlingboolean[socketCount] = false;
	}

	class SocketHandling {
		CommandHandler commandHandler;

		public SocketHandling(CommandHandler handler, final OutputStreamWriter writer, final BufferedReader reader,
				final int socketCount) {
			commandHandler = handler;
			new Thread(new Runnable() {
				public void run() {
					sockethandlingboolean[socketCount - 1] = false;
					sockethandlingboolean[socketCount] = true;
					addlog("SocketCount: " + socketCount);
					currentClient = remoteName;
					addlog("Connected to: " + currentClient);
					addClientList(currentClient);
					if (!daemonMode) {
						Gui.getInstance().display.asyncExec(new Runnable() {
							public void run() {
								Gui.getInstance().setSubItemText(currentClient);
							}
						});
					}
					try {
						String cmd;
						while (null != (cmd = reader.readLine()) && sockethandlingboolean[socketCount]) {
							addlog("got command: " + cmd);
							String result = commandHandler.processCommand(cmd);
							if (result == null) {
								addlog("send result: " + RESULT_OK);
								writer.write(RESULT_OK);
							} else {
								addlog("send result: " + result);
								writer.write(result);
							}
							writer.write("\n");
							writer.flush();
						}
						if(cmd == null)
							System.out.println("Verbindung durch client beendet.");
						else {
							System.out.println("Verbindung beendet...");
						}
						if (!sockethandlingboolean[socketCount]) {
							writer.write("kicked, new client");
							writer.write("\n");
							writer.flush();
							addlog("Connection closed"); 
							writer.close();
							reader.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}

	}

	public void setDaemonMode(boolean daemonModetmp) {
		this.daemonMode = daemonModetmp;
	}

	public boolean isConnected() {
		return currentSocket != null && currentSocket.isConnected();
	}

	public boolean[] getSocketHandlingBoolean() {
		return sockethandlingboolean;
	}

	public int getSocketCount() {
		return socketCount;
	}

	public ArrayList<String> getClientList() {
		return clientList;
	}

	public ArrayList<String> getLogList() {
		return logList;
	}

	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public void setPassword(String passwordtmp) {
		password = passwordtmp;
	}

	public static SocketServerTransport getInstance() {
		if (socketServerTransport == null)
			socketServerTransport = new SocketServerTransport();
		return socketServerTransport;
	}
}
