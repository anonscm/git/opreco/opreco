/*
 * opreco,
 * open presentation control
 * Copyright (c) 2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'opreco'
 * Signature of Elmar Geese, 11 March 2008
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.oprecoserver;

import java.io.File;

public class Main {

	/**
	 * Start the opreco server
	 * 
	 * @throws SocketException
	 */
	public static String homepath;

	public static void main(String[] args) {

		homepath = System.getProperty("user.home") + "/.opreco";
		File oprecoFolder = new File(homepath);
		if (!oprecoFolder.exists())
			oprecoFolder.mkdir();

		if (args.length != 0) {
			if (args[0].equals("--server")) {
				ServerTransport transport = SocketServerTransport.getInstance();
				Server server = new Server(transport);
				server.start();
			}else if (args[0].equals("--daemon")) {
				ServerTransport transport = null;
				SocketServerTransport.getInstance().setDaemonMode(true);
				if (args.length > 1) {
					SocketServerTransport.getInstance().setPassword(args[1]);
					transport = SocketServerTransport.getInstance();
					Server server = new Server(transport);
					server.start(true);
				} else {
					printInformation();
				}
			}else{
				printInformation();		
			}
		} else {
			printInformation();
		}
	}
	
	public static void printInformation(){
		System.out.println("Opreco 0.3.0 is a Linux based presentation");
		System.out.println("control for OpenMoko and Maemo based devices");
		System.out.println("published under the GNU General Public License 3.0.");
		System.out.println("For further information see http://wiki.evolvis.org/opreco/");
		System.out.println();
		System.out.println("usage:");
		System.out.println("	oprecoserver --server");
		System.out.println("	oprecoserver --daemon <password>");
	}

}
