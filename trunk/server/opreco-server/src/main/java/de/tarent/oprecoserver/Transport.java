package de.tarent.oprecoserver;

import java.io.IOException;

public interface Transport {

	public static final String RESULT_OK = "ok.";
	public static final String SERVER_QUIT = "server:quit";
	
	/**
	 * close the connection
	 */
	public void close() throws IOException;
	 
	/**
	 * returns true, if the transport is currently connected to another endpoint. 
	 * (e.g. client with server or server with client)
	 */
	public boolean isConnected();

}
