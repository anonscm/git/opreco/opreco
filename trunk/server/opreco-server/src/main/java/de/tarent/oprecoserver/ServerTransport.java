package de.tarent.oprecoserver;

import java.io.IOException;

public interface ServerTransport extends Transport {
	
	/**
	 * Listen for incoming requests.
	 * @throws IOException
	 */
	public void listen(CommandHandler handler) throws IOException;
	
	/** 
	 * Call back interface for command handler
	 */
	public interface CommandHandler {
		/**
		 * Handle a command
		 * @return the result
		 * @throws IOException 
		 */
		public String processCommand(String cmd) throws IOException;
	}
}
