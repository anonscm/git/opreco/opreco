package de.tarent.opreco;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

public class ConnectionDialog {

	Shell dialog;

	ClientTransport selectedTransport;

	SocketClientTransport socketTransport;

	MotionControl motionControl;

	Text hostEntry;

	Text portEntry;

	Text passwordEntry;

	Text btPasswordEntry;

	List list;

	static String password;

	boolean motionStatus = false;

	boolean statusOk;

	boolean refresh_pressed = false;

	public void motionControlStart() {
		if ((!MotionControl.status)) {
			motionControl = new MotionControl(socketTransport);
			MotionControl.status = true;
		}
	}

	Listener connectListener = new Listener() {
		public void handleEvent(Event arg0) {
			try {
				socketTransport = (SocketClientTransport) selectedTransport;
				socketTransport.setHost(hostEntry.getText());
				socketTransport.setPort(Integer.parseInt(portEntry.getText()));
				socketTransport.setPassword(passwordEntry.getText());
				motionControlStart();
				motionControl.start();

				statusOk = true;
				dialog.dispose();
			} catch (NumberFormatException nfe) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("wrong value");
				mb.setMessage("The port should be a number!");
				mb.open();
			}
		}
	};

	Listener bluetoothConnectButtonListener = new Listener() {
		public void handleEvent(Event arg0) {
			bluetoothConnect();
		}
	};

	Listener cancelListener = new Listener() {
		public void handleEvent(Event arg0) {
			statusOk = false;
			dialog.dispose();
		}
	};

	public void initConnectionDialog(Shell parent) {
		
		dialog = new Shell(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		dialog.setText("Connection");
		dialog.setSize(500, 500);
		GridLayout dialogLayout = new GridLayout(2, false);
		dialogLayout.horizontalSpacing = 20;
		dialogLayout.verticalSpacing = 20;
		dialog.setLayout(dialogLayout);

		final TabFolder tabFolder = new TabFolder(dialog, SWT.NONE);
		TabItem bluetooth = new TabItem(tabFolder, SWT.NONE);
		bluetooth.setText("Bluetooth");
		TabItem lan = new TabItem(tabFolder, SWT.NONE);
		lan.setText("LAN/W-LAN");
		bluetooth.addListener(SWT.Selection, bluetoothListener);

		// socketConfiguration
		Composite socketConfiguration = new Composite(tabFolder, SWT.NONE);
		socketConfiguration.setLayout(new GridLayout(2, false));
		socketConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
				true, 2, 1));

		hostEntry = appendFormEntry(socketConfiguration, "Host");
		portEntry = appendFormEntry(socketConfiguration, "Port");
		passwordEntry = appendFormEntry(socketConfiguration, "Password");

		// button bar
		Button cancelButton = new Button(socketConfiguration, SWT.NONE);
		GridData cancelGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false,
				false);
		cancelGridData.widthHint = 100;
		cancelGridData.heightHint = 100;
		cancelButton.setLayoutData(cancelGridData);
		cancelButton.setText("Cancel");
		cancelButton.addListener(SWT.Selection, cancelListener);

		Button connectButton = new Button(socketConfiguration, SWT.NONE);
		GridData connectGridData = new GridData(SWT.RIGHT, SWT.BOTTOM, false,
				false);
		connectGridData.widthHint = 100;
		connectGridData.heightHint = 100;
		connectButton.setLayoutData(connectGridData);
		connectButton.setText("Connect");
		connectButton.addListener(SWT.Selection, connectListener);

		lan.setControl(socketConfiguration);

		// BluetoothConfiguration

		Composite bluetoothConfiguration = new Composite(tabFolder, SWT.NONE);
		bluetoothConfiguration.setLayout(new GridLayout(2, false));
		bluetoothConfiguration.setLayoutData(new GridData(SWT.FILL, SWT.TOP,
				true, true, 2, 1));

		Composite buttonComposite = new Composite(bluetoothConfiguration,
				SWT.NONE);
		buttonComposite.setLayout(new GridLayout(2, false));
		buttonComposite.setLayoutData(new GridData(SWT.FILL, SWT.RIGHT, true,
				true, 2, 1));

		Composite passwordComposite = new Composite(bluetoothConfiguration,
				SWT.NONE);
		passwordComposite.setLayout(new GridLayout(2, false));
		passwordComposite.setLayoutData(new GridData(SWT.FILL, SWT.RIGHT, true,
				true, 2, 1));

		list = new List(bluetoothConfiguration, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL);
		GridData bluetoothConfigurationGridData = new GridData(SWT.RIGHT,
				SWT.BOTTOM, false, false);
		bluetoothConfigurationGridData.widthHint = 300;
		bluetoothConfigurationGridData.heightHint = 250;
		list.setLayoutData(bluetoothConfigurationGridData);

		Button bluetoothConnectButton = new Button(buttonComposite, SWT.PUSH);
		GridData bluetoothConnectButtonGridData = new GridData(SWT.RIGHT,
				SWT.BOTTOM, false, false);
		bluetoothConnectButtonGridData.widthHint = 150;
		bluetoothConnectButtonGridData.heightHint = 50;
		bluetoothConnectButton.setLayoutData(bluetoothConnectButtonGridData);
		bluetoothConnectButton.setText("Connect");
		bluetoothConnectButton.addListener(SWT.Selection,
				bluetoothConnectButtonListener);

		Button refreshButton = new Button(buttonComposite, SWT.PUSH);
		GridData refreshButtonGridData = new GridData(SWT.RIGHT, SWT.BOTTOM,
				false, false);
		refreshButtonGridData.widthHint = 150;
		refreshButtonGridData.heightHint = 50;
		refreshButton.setLayoutData(refreshButtonGridData);
		refreshButton.setText("Refresh");
		refreshButton.addListener(SWT.Selection, bluetoothListener);

		Label passwordLabel = new Label(passwordComposite, SWT.NONE);
		passwordLabel.setText("Password:");

		btPasswordEntry = new Text(passwordComposite, SWT.BORDER | SWT.MULTI);
		GridData btPasswordEntryGridData = new GridData(SWT.LEFT, SWT.TOP,
				false, false);
		btPasswordEntryGridData.widthHint = 240;
		btPasswordEntryGridData.heightHint = 20;
		btPasswordEntry.setLayoutData(btPasswordEntryGridData);
		btPasswordEntry.setText("1234");

		bluetooth.setControl(bluetoothConfiguration);

		clearListAsync();
		String[] defaults = getBTDefaults();
		if (defaults[0] != null) {
			setNewContentAsync("last connection");
		}
		dialog.pack();
		open();

	}

	public ConnectionDialog(Shell parent, ClientTransport preSelectedTransport,
			boolean allowAutoConnect) {
		selectedTransport = preSelectedTransport;
		if (allowAutoConnect) {
			String[] defaults = getBTDefaults();
			if ((defaults[2] != null)
					&& (allowAutoConnect && defaults[2].equals("autoconnect"))) {
//				list.add("last connection"); // fix
				if (!bluetoothConnect()) {
					initConnectionDialog(parent);
				}
			}
			else{
				initConnectionDialog(parent);
			}
		} else
			initConnectionDialog(parent);
	}

	Process proc = null;

	private void btsearch() {
		new Thread(new Runnable() {
			public void run() {
				String first = "";
				System.out.println("Searching for Bluetooth devices");
				try {
					clearListAsync();
					setNewContentAsync("Searching...");
					proc = Runtime.getRuntime().exec("hcitool scan");
				} catch (IOException e) {
					e.printStackTrace();
				}
				BufferedReader in = new BufferedReader(new InputStreamReader(
						proc.getInputStream()));
				String[] test = new String[100];
				int i = 0;
				try {
					first = in.readLine();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				System.out.println(first);
				System.out.println("Search finished");
				while (true) {
					try {
						test[i] = in.readLine();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (test[i] == null) {
						if (i == 0) {
							setNewContentAsync("Nothing found");
						}
						break;
					}
					i++;
				}
				clearListAsync();
				for (int e = 0; e < i; e++) {
					if (test[e] != null) {
						setNewContentAsync(test[e]);
						System.out.println(test[e]);
					}
				}
				if (first == null) {
					setNewContentAsync("No powered bluetooth");
					setNewContentAsync("device found.");
				}
			}
		}).start();
	}

	public void setNewContent(String string) {
		list.add(string);
	}

	public void clearListAsync() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				list.removeAll();
			}
		});
	}

	public void setNewContentAsync(final String string) {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				setNewContent(string);
			}
		});
	}

	Listener bluetoothListener = new Listener() {
		public void handleEvent(Event arg0) {
			System.out.println("BluetoothListener");
			refresh_pressed = true;
			btsearch();
		}
	};

	public boolean isStatusOk() {
		return statusOk;
	}

	/**
	 * @param socketConfiguration
	 */
	private Text appendFormEntry(Composite socketConfiguration, String label) {
		Label labelComponent = new Label(socketConfiguration, SWT.NONE);
		labelComponent.setText(label);
		Text entry = new Text(socketConfiguration, SWT.SINGLE);
		GridData entryLayout = new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1);
		entryLayout.widthHint = 300;
		entry.setLayoutData(entryLayout);
		return entry;
	}

	/**
	 * 
	 * @param currentTransport
	 *            the transport currently used, may be null
	 */
	private void open() {
		socketTransport = (SocketClientTransport) selectedTransport;
		hostEntry.setText(saveNull(socketTransport.getHost()));
		portEntry.setText("" + socketTransport.getPort());
		passwordEntry.setText(saveNull(socketTransport.getPassword()));

		statusOk = false;
		dialog.open();
		Display display;
		if (Display.getCurrent() == null)
			display = new Display();
		else
			display = Display.getCurrent();
		while (!dialog.isDisposed())
			display.readAndDispatch();
	}

	private String saveNull(String string) {
		return string != null ? string : "";
	}

	public ClientTransport getSelectedTransport() {
		return selectedTransport;
	}

	private String[] getBTDefaults() {
		try {
			
			String filepath = System.getProperty("user.home")+"/.opreco/";
			File a = new File(filepath + "connection.settings");
			FileReader fr = new FileReader(a);
			BufferedReader br = new BufferedReader(fr);

			String mac = br.readLine();
			String pw = br.readLine();
			String autoconnect = br.readLine();
			br.close();
			String[] defaults = new String[3];
			defaults[0] = mac;
			defaults[1] = pw;
			defaults[2] = autoconnect;
			return defaults;

		} catch (IOException e) {
			System.err.println("Could not read connection settings from file.");
		}
		String[] error = new String[3];
		error[0] = null;
		error[1] = null;
		error[2] = null;
		return error;
	}

	boolean bluetoothConnect() {		
		String[] test = null;
		try {
			test = list.getSelection();
		}
		catch (Exception e) {
		}

		if ((refresh_pressed) && (test.length == 0)) {
			MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
			mb.setText("Error");
			mb.setMessage("Please scan for devices and choose");
			mb.open();
			return false;
		} else {

			String mac = "";
			String pw = "";
			String autoconnect = "";
			if (!refresh_pressed) {
				String[] defaults = getBTDefaults();
				mac = defaults[0];
				pw = defaults[1];
				autoconnect = defaults[2];
			} else {
				pw = btPasswordEntry.getText();
				for (int i = 1; i < 18; i++) {
					mac = mac + test[0].charAt(i);
				}
			}
			try {
				Runtime.getRuntime().exec("pand --killall");
				Runtime.getRuntime().exec(
						"pand --connect " + mac + " --autozap");
				System.out.println("pand connected!");
			} catch (IOException e) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("Error");
				mb.setMessage("Error PAN setup");
				mb.open();
				e.printStackTrace();
			}
			Process ifconfig = null;
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

			try {
				Process ifconfig2 = Runtime.getRuntime().exec(
						"ifconfig bnep0 172.16.0.254");
				BufferedReader in2 = new BufferedReader(new InputStreamReader(
						ifconfig2.getInputStream()));
				System.out.println(in2.readLine());
				for (int i = 2; i < 254; i++) {

					ifconfig = Runtime.getRuntime().exec(
							"ping -c 1 172.16.0." + i);

					BufferedReader in = new BufferedReader(
							new InputStreamReader(ifconfig.getInputStream()));
					// System.out.println(in.ready());
					// in.readLine();
					String temp = in.readLine();
					System.out.println(temp);
					temp = in.readLine();
					System.out.println(temp);
					temp = in.readLine();
					System.out.println(temp);
					temp = in.readLine();
					System.out.println(temp);

					if (temp != null) {
						if (temp.contains("100% packet loss")) {
							System.out.println("bnep0: 172.16.0." + i);
							Runtime.getRuntime().exec(
									"ifconfig bnep0 172.16.0." + i);
							break;
						}
					}
					if (i > 10) {
						MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
						mb.setText("Error");
						mb.setMessage("Connection failed");
						mb.open();
						return false;
					}
				}

			} catch (IOException e) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("Error");
				mb.setMessage("Error during interface setup");
				mb.open();
				e.printStackTrace();
			}
			try {
				socketTransport = (SocketClientTransport) selectedTransport;
				socketTransport.setHost("172.16.0.1");
				socketTransport.setPort(2000);
				socketTransport.setPassword(pw);

				motionControlStart();
				MotionControl.start();

				try {
					Process whoami = Runtime.getRuntime().exec("whoami");
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(whoami.getInputStream()));
					String user = reader.readLine();
					String filepath = "/home/" + user + "/.opreco/";

					File a = new File(filepath + "connection.settings");
					FileWriter fw = null;
					fw = new FileWriter(a);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(mac);
					bw.write("\n" + pw);
					bw.write("\n" + autoconnect);
					bw.close();
				} catch (IOException e) {
					System.err
							.println("Could not write connection settings to file.");
				}

				statusOk = true;
				if(dialog != null)
					dialog.dispose();
				return true;
			} catch (NumberFormatException nfe) {
				MessageBox mb = new MessageBox(dialog, SWT.ICON_WARNING);
				mb.setText("wrong value");
				mb.setMessage("The port should be a number!");
				mb.open();
			}
			return false;
		}
	}

}
