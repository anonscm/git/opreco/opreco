package de.tarent.opreco;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Text;

/*
 * Draft
 * Funktionalität:
 * - Button Views für verschiedene Applikationssets
 * - Connection Status, Connect, Disconnect, Shutdown Server
 * - Connection und Transport Konfiguration 
 *
 */

public class ControlClient {

	Display display;

	Shell shell;

	Text status;

	Button button;

	Scale scale;

	ClientTransport transport;

	Image imageFullscreen = null;
	Image imageNoFullscreen = null;

	Button btFS = null;

	MenuItem saveConn = null;

	Listener closeListener = new Listener() {
		public void handleEvent(Event arg0) {
			try {
				transport.close();
			} catch (IOException e) {
				System.out.println("error on closing the connection");
				e.printStackTrace();
			}
			try {
				Runtime.getRuntime().exec("pand --killall");
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.exit(1);
			shell.dispose();
		}
	};

	Listener connectionListener = new Listener() {
		public void handleEvent(Event arg0) {
			openConnectionDialog(false);
		}
	};

	Listener disConnectionListener = new Listener() {
		public void handleEvent(Event arg0) {
			try {
				transport.close();
			} catch (IOException e) {
				// TODO: how to handle?
			}
		}
	};

	Listener saveConnListener = new Listener() {
		public void handleEvent(Event arg0) {
			Process whoami;
			File a = null;

			try {
				whoami = Runtime.getRuntime().exec("whoami");

				BufferedReader reader = new BufferedReader(
						new InputStreamReader(whoami.getInputStream()));
				String user = reader.readLine();
				String filepath = "/home/" + user + "/.opreco/";
				a = new File(filepath + "connection.settings");
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			String mac = "";
			String pw = "";
			try {

				FileReader fr = new FileReader(a);
				BufferedReader br = new BufferedReader(fr);
				mac = br.readLine();
				pw = br.readLine();
				br.close();
			} catch (IOException e) {
				System.err
						.println("Could not read connection settings from file.");
			}
			try {

				FileWriter fw = new FileWriter(a);
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(mac);
				bw.write("\n" + pw);
				if (saveConn.getSelection())
					bw.write("\nautoconnect");
				else
					bw.write("\nno autoconnect");
				bw.close();
			} catch (IOException e) {
				System.err
						.println("Could not write connection settings to file.");
			}

		}
	};

	Listener scaleChangedListener = new Listener() {
		public void handleEvent(Event arg0) {
			MotionControl.setChange(scale.getSelection());
			System.out.println(scale.getSelection());
		}
	};

	public ControlClient(ClientTransport transport) {
		this.transport = transport;
		createGui();
		createMenu();
	}

	public void start() {
		// open the main window
		shell.open();
		openConnectionDialog(true);
		// do gui dispatching until the main window is closed
		while (!shell.isDisposed())
			display.readAndDispatch();
	}

	private void openConnectionDialog(boolean allowAutoConnect) {
	//	System.out.println("void openConnectionDialog called");
		ConnectionDialog connectionDialog = new ConnectionDialog(shell,
				transport, allowAutoConnect);
		System.out.println("connectionDialog.isStatusOk(): "
				+ connectionDialog.isStatusOk());
		if (connectionDialog.isStatusOk()) {
			transport = connectionDialog.getSelectedTransport();
			connect();
		}
	}

	public void openConnectionDialogAsync() {
		display.asyncExec(new Runnable() {
			public void run() {
				openConnectionDialog(false);
			}
		});
	}

	public void publishErrorAsync(final String title, final String text) {
		display.asyncExec(new Runnable() {
			public void run() {
				MessageBox mb = new MessageBox(shell, SWT.ICON_ERROR);
				mb.setText(title);
				mb.setMessage(text);
				mb.open();
			}
		});
	}

	private void connect() {
		new Thread(new Runnable() {
			public void run() {
				setStatusAsync("connecting to " + transport + " ...");
				try {
					if (transport.isConnected())
						transport.close();
					if (!transport.connect()) {
						openConnectionDialogAsync();
						publishErrorAsync("Bad Password",
								"Entered wrong password, try again");
					}
				} catch (IOException e) {
					e.printStackTrace();
					setStatusAsync("connection error");
					publishErrorAsync("connection error", e.getMessage());
				}
				setStatusAsync("connected with " + transport + "!");
			}
		}).start();
	}

	public void setStatus(String string) {
		status.setText(string);
	}

	public void setStatusAsync(final String string) {
		display.asyncExec(new Runnable() {
			public void run() {
				setStatus(string);
			}
		});
	}

	private void createGui() {
		display = Display.getDefault();
		shell = new Shell(display);
		shell.setText("Opreco");
		shell.setFullScreen(true);

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		shell.setLayout(gridLayout);

		Control keyPanel = null;
		if (true == "Windows CE".equals(System.getProperty("os.name"))) {
			keyPanel = createKeyPanelWinCE(shell);
		} else {
			keyPanel = createKeyPanel(shell);
		}
		keyPanel.setLayoutData(new GridData(GridData.FILL_BOTH));

		status = new Text(shell, SWT.SINGLE);
		status.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		status.setEditable(false);
		status.setText("");

		Display.getCurrent().addFilter(SWT.KeyDown, new Listener() {
			public void handleEvent(Event event) {
				if (event.keyCode == SWT.F6) {
					if (shell.getFullScreen())
						btFS.setImage(imageFullscreen);
					else
						btFS.setImage(imageNoFullscreen);

					shell.setFullScreen(!shell.getFullScreen());
				}
				if (event.keyCode == SWT.F7) {
				
					if (!transport.isConnected()) {
						openConnectionDialog(false);
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							// ignore
						}
						if (!transport.isConnected())
							return;
					}
					new Thread(new Runnable() {
						public void run() {
							String cmd = "key Right";
							setStatusAsync("sending " + cmd + "...");
							try {
								transport.sendCommand(cmd);
							} catch (IOException e) {
								e.printStackTrace();
								setStatusAsync("sending error");
								publishErrorAsync("sending error", e.getMessage());
							}
							setStatusAsync("sent " + cmd + "");
						}
					}).start();
				}
				if (event.keyCode == SWT.F8) {
					if (!transport.isConnected()) {
						openConnectionDialog(false);
						try {
							Thread.sleep(300);
						} catch (InterruptedException e) {
							// ignore
						}
						if (!transport.isConnected())
							return;
					}
					new Thread(new Runnable() {
						public void run() {
							String cmd = "key Left";
							setStatusAsync("sending " + cmd + "...");
							try {
								transport.sendCommand(cmd);
							} catch (IOException e) {
								e.printStackTrace();
								setStatusAsync("sending error");
								publishErrorAsync("sending error", e.getMessage());
							}
							setStatusAsync("sent " + cmd + "");
						}
					}).start();
				}
			}
		});
	}

	private Control createKeyPanelWinCE(Composite parent) {
		Composite buttonComposite = new Composite(parent, SWT.NONE);
		FillLayout fillLayout = new FillLayout(SWT.VERTICAL);
		fillLayout.spacing = 10;
		buttonComposite.setLayout(fillLayout);
		createCommandButton(buttonComposite, new Command("<-", "key Left"));
		createCommandButton(buttonComposite, new Command("->", "key Right"));
		return buttonComposite;
	}

	/**
	 * 
	 */
	private Control createKeyPanel(Composite parent) {
		TabFolder tabFolder = new TabFolder(parent, SWT.BORDER);
		Font fontTab = new Font(display, "Arial", 16, SWT.BOLD);
		tabFolder.setFont(fontTab);
		Composite buttonComposite = new Composite(tabFolder, SWT.BEGINNING);

		GridData gridData1;
		GridData gridDataCompLeft = null;
		GridData gridDataCompRight = null;

		Composite compLeft = null;
		Composite compRight = null;

		GridLayout layout;
		
		String path = "/usr/share/icons/hicolor/32x32/apps/"; //"./src/auxfiles/opreco/32x32/";
		
		
		if (display.getBounds().width > display.getBounds().height) {
			layout = new GridLayout(3, false);
			gridData1 = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 8);

			buttonComposite.setLayout(layout);

			TabItem tabItem = new TabItem(tabFolder, SWT.BEGINNING);
			tabItem.setText("Navigation");
			tabItem.setControl(buttonComposite);

			scale = new Scale(buttonComposite, SWT.VERTICAL);
			scale.addListener(SWT.Selection, scaleChangedListener);

			GridData gridScale = new GridData(SWT.CENTER, SWT.FILL, false,
					true, 1, 7);
			scale.setLayoutData(gridScale);
			scale.setMaximum(1000);
			scale.setSelection(500);

			createCommandButton(buttonComposite, new Command("<-", "key Left"))
					.setLayoutData(gridData1);

			createCommandButton(buttonComposite, new Command("->", "key Right"))
					.setLayoutData(gridData1);

			btFS = new Button(buttonComposite, SWT.NONE);
			GridData gridFS = new GridData(SWT.BEGINNING, SWT.END, false,
					false, 1, 1);
			btFS.setLayoutData(gridFS);
			try {
				imageFullscreen = new Image(display, path + "fullscreen.png");
				imageNoFullscreen = new Image(display, path
						+ "nofullscreen.png");
				btFS.setImage(imageFullscreen);
			} catch (Exception e) {
				btFS.setText("FS");
			}
			btFS.addListener(SWT.Selection, new Listener() {
				public void handleEvent(Event arg0) {
					if (shell.getFullScreen())
						btFS.setImage(imageFullscreen);
					else
						btFS.setImage(imageNoFullscreen);
					shell.setFullScreen(!shell.getFullScreen());
				}
			});
		} else {
			layout = new GridLayout(12, false);
			// gridData1 = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 4);

			gridDataCompLeft = new GridData(SWT.BEGINNING, SWT.FILL, false,
					true, 1, 1);
			gridDataCompRight = new GridData(SWT.FILL, SWT.FILL, true, true,
					11, 1);
			compLeft = new Composite(buttonComposite, SWT.NONE);
			compRight = new Composite(buttonComposite, SWT.NONE);

			GridLayout layoutComp = new GridLayout(1, false);
			compLeft.setLayout(layoutComp);
			compRight.setLayout(layoutComp);

			compLeft.setLayoutData(gridDataCompLeft);
			compRight.setLayoutData(gridDataCompRight);

			// layout.horizontalSpacing = 8;
			// layout.verticalSpacing = 2;
			buttonComposite.setLayout(layout);

			TabItem tabItem = new TabItem(tabFolder, SWT.BEGINNING);
			tabItem.setText("Navigation");
			tabItem.setControl(buttonComposite);

			scale = new Scale(compLeft, SWT.VERTICAL);
			scale.addListener(SWT.Selection, scaleChangedListener);

			GridData gridScale = new GridData(SWT.CENTER, SWT.FILL, false,
					true, 1, 7);
			scale.setLayoutData(gridDataCompLeft);
			scale.setMaximum(1000);
			scale.setSelection(500);

			createCommandButton(compRight, new Command("<-", "key Left"))
					.setLayoutData(gridDataCompRight);

			createCommandButton(compRight, new Command("->", "key Right"))
					.setLayoutData(gridDataCompRight);

			final Button btFS = new Button(compLeft, SWT.NONE);
			GridData gridFS = new GridData(SWT.CENTER, SWT.BOTTOM, false,
					false, 1, 1);
			btFS.setLayoutData(gridFS);
			try {
				imageFullscreen = new Image(display, path + "fullscreen.png");
				imageNoFullscreen = new Image(display, path
						+ "nofullscreen.png");
				btFS.setImage(imageFullscreen);
			} catch (Exception e) {
				btFS.setText("FS");
			}
			btFS.addListener(SWT.Selection, new Listener() {
				public void handleEvent(Event arg0) {
					if (shell.getFullScreen())
						btFS.setImage(imageFullscreen);
					else
						btFS.setImage(imageNoFullscreen);
					shell.setFullScreen(!shell.getFullScreen());
				}
			});
		}

		Composite buttonComposite2 = new Composite(tabFolder, SWT.NONE);
		GridLayout layout2 = new GridLayout(2, true);
		layout2.horizontalSpacing = 10;
		layout2.verticalSpacing = 10;
		buttonComposite2.setLayout(layout2);
		TabItem tabItem2 = new TabItem(tabFolder, SWT.CENTER);
		tabItem2.setText("Slides");
		tabItem2.setControl(buttonComposite2);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		createCommandButton(buttonComposite2, new Command("Esc", "key Escape"))
				.setLayoutData(
						new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		createCommandButton(
				buttonComposite2,
				new Command("CTRL-L", "keydown Control_L;key l;keyup Control_L"))
				.setLayoutData(gridData);
		createCommandButton(buttonComposite2, new Command("F5", "key F5"))
				.setLayoutData(gridData);
		createCommandButton(buttonComposite2, new Command("Home", "key Home"))
				.setLayoutData(gridData);
		createCommandButton(buttonComposite2,
				new Command("PgUp", "key Page_Up")).setLayoutData(gridData);
		createCommandButton(buttonComposite2, new Command("End", "key End"))
				.setLayoutData(gridData);
		createCommandButton(buttonComposite2,
				new Command("PgDn", "key Page_Down")).setLayoutData(gridData);
		return tabFolder;
	}

	Button createCommandButton(Composite parent, Command cmd) {
		Button button = new Button(parent, SWT.NONE);
		button.setText(cmd.getLabel());
		button.addListener(SWT.Selection, cmd);
		return button;
	}

	private void createMenu() {
		Menu bar = new Menu(shell, SWT.BAR);
		shell.setMenuBar(bar);

		MenuItem fileMenuItem = new MenuItem(bar, SWT.CASCADE);
		fileMenuItem.setText("File");
		Menu fileMenu = new Menu(shell, SWT.DROP_DOWN);
		fileMenuItem.setMenu(fileMenu);

		MenuItem connect = new MenuItem(fileMenu, SWT.PUSH);
		connect.setText("Connect");
		connect.addListener(SWT.Selection, connectionListener);

		MenuItem disConnect = new MenuItem(fileMenu, SWT.PUSH);
		disConnect.setText("Disconnect");
		disConnect.addListener(SWT.Selection, disConnectionListener);

		MenuItem quit = new MenuItem(fileMenu, SWT.NONE);
		quit.setText("Quit");
		quit.addListener(SWT.Selection, closeListener);

		MenuItem optionsMenuItem = new MenuItem(bar, SWT.CASCADE);
		optionsMenuItem.setText("Options");
		Menu optionsMenu = new Menu(shell, SWT.DROP_DOWN);
		optionsMenuItem.setMenu(optionsMenu);

		saveConn = new MenuItem(optionsMenu, SWT.CHECK);
		saveConn.setText("Save last Conn.");
		saveConn.addListener(SWT.Selection, saveConnListener);
		saveConn.setSelection(false);
		try {
			Process whoami = Runtime.getRuntime().exec("whoami");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					whoami.getInputStream()));
			String user = reader.readLine();
			String filepath = "/home/" + user + "/.opreco/";
			File a = new File(filepath + "connection.settings");
			FileReader fr = new FileReader(a);
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			br.readLine();
			String temp = br.readLine();
			if (temp != null)
				if (temp.equals("autoconnect"))
					saveConn.setSelection(true);

			br.close();
		} catch (IOException e) {
			System.err
					.println("Could not read autoconnection settings from file.");
		}

	}

	public class Command implements Listener {
		String cmd;

		String label;

		public Command(String label, String cmd) {
			this.cmd = cmd;
			this.label = label;
		}

		public void handleEvent(Event arg0) {
			if (!transport.isConnected()) {
				openConnectionDialog(false);
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					// ignore
				}
				if (!transport.isConnected())
					return;
			}
			new Thread(new Runnable() {
				public void run() {
					setStatusAsync("sending " + cmd + "...");
					try {
						transport.sendCommand(cmd);
					} catch (IOException e) {
						e.printStackTrace();
						setStatusAsync("sending error");
						publishErrorAsync("sending error", e.getMessage());
					}
					setStatusAsync("sent " + cmd + "");
				}
			}).start();
		}

		public String getCmd() {
			return cmd;
		}

		public void setCmd(String cmd) {
			this.cmd = cmd;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}
	};

}
