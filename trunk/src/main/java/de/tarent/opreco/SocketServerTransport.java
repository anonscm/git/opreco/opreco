package de.tarent.opreco;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolTip;
import org.eclipse.swt.widgets.Tray;
import org.eclipse.swt.widgets.TrayItem;

public class SocketServerTransport implements ServerTransport {

	int port;
	ServerSocket serverSocket;
	Socket currentSocket;
	boolean sockethandlingboolean[] = new boolean[100];
	static String password = "";
	int socketCount = 1;
	ArrayList<String> logList = new ArrayList<String>();
	ArrayList<String> clientList = new ArrayList<String>();
	String currentClient = "No client connected";
	public MenuItem subItem1;
	String username;

	boolean noSWT = false;

	public void addlog(String message) {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss ");
		Date currentTime = new Date();
		logList.add(formatter.format(currentTime) + ": " + message);
		System.out.println(formatter.format(currentTime) + ": " + message);
	}

	public void addClientList(String message) {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss ");
		Date currentTime = new Date();
		clientList.add(formatter.format(currentTime) + ": " + message);
	}

	private void btlisten() {
		new Thread(new Runnable() {
			public void run() {
				try {
					String filepath = System.getProperty("user.home")+"/.opreco/";
					String username = System.getProperty("user.name");
					
					File a = new File(filepath + "bnep0.sh");
					FileWriter fw = null;
					fw = new FileWriter(a);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write("#!/bin/sh\n");
					bw.write("sleep 1\n");
					bw.write("ifconfig bnep0 172.16.0.1");
					bw.close();

					File a2 = new File(filepath + "pand.sh");
					FileWriter fw2 = null;
					fw2 = new FileWriter(a2);
					BufferedWriter bw2 = new BufferedWriter(fw2);
					bw2.write("#!/bin/sh\n");
					bw2.write("pand --killall\n");
					bw2.write("pand --listen --role GN --devup " + filepath + "bnep0.sh");
					bw2.close();

					Runtime.getRuntime().exec(
							"chmod +x " + filepath + "bnep0.sh");
					
					while (username == null) {

					}

					
					
					if (!username.equals("iamroot")) {
						try {
							Process scriptKdesu = Runtime.getRuntime().exec(
									"kdesu sh " + filepath + "pand.sh");
						} catch (IOException e) {
							try {
								Process scriptGksu = Runtime.getRuntime().exec(
										"gksu sh " + filepath + "pand.sh");
							} catch (IOException f) {
								System.out
										.println("Install kdesu, gksu or run as root");
								System.exit(1);
							}

						}
					} else {
						Process script = Runtime.getRuntime().exec(
								"sh " + filepath + "pand.sh");
					}
					addlog("Setting up PAN Server");
				} catch (IOException e) {
					addlog("Error on pand --listen");
					e.printStackTrace();
				}
			}
		}).start();
	}

	public SocketServerTransport(int port, String pw) {
		this.noSWT = true;
		password = pw;
		this.port = port;
	}

	public SocketServerTransport(int port) {
		this.port = port;
	}

	public synchronized void listen(CommandHandler handler) throws IOException {
		addlog("opening server");
		if (!noSWT) {
			TrayIcon trayIcon = new TrayIcon();
		}
		serverSocket = new ServerSocket(port);
		btlisten();

		// accept any connection at a time

		while (!serverSocket.isClosed()) {
			addlog("accepting on port " + port);
			Socket socket = new Socket();
			socket = serverSocket.accept();
			addlog("got connection from " + socket.getRemoteSocketAddress());
			addClientList("" + socket.getRemoteSocketAddress());
			OutputStreamWriter writer;
			writer = new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
			BufferedReader reader;
			reader = new BufferedReader(new InputStreamReader(socket
					.getInputStream()));

			String random = String.valueOf(Math.floor(Math.random() * 100) + 1);
			writer.write(random);
			writer.write("\n");
			writer.flush();

			String md5 = ChecksumTool.createChecksum(password + random);
			System.out.println(md5);

			String incommingMD5 = null;
			incommingMD5 = reader.readLine();
			if (incommingMD5.equals(md5)) {
				SocketHandling socketHandling = new SocketHandling(handler,
						socket, socketCount);
				socketCount++;
			} else {
				writer.write("Bad Password");
				writer.write("\n");
				writer.flush();
			}
		}
	}

	public void close() throws IOException {
		serverSocket.close();
	}

	public boolean isConnected() {
		return currentSocket != null && currentSocket.isConnected();
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	class SocketHandling {
		CommandHandler commandHandler;

		public SocketHandling(CommandHandler handler, final Socket socket,
				final int socketCount) {
			commandHandler = handler;
			new Thread(new Runnable() {
				public void run() {
					sockethandlingboolean[socketCount - 1] = false;
					sockethandlingboolean[socketCount] = true;
					addlog("SocketCount: " + socketCount);
					currentClient = "" + socket.getRemoteSocketAddress();
					if (!noSWT) {
						Display.getDefault().asyncExec(new Runnable() {
							public void run() {
								subItem1.setText(currentClient);
							}
						});
					}
					OutputStreamWriter writer = null;
					BufferedReader reader = null;
					try {
						writer = new OutputStreamWriter(socket
								.getOutputStream(), "UTF-8");
						reader = new BufferedReader(new InputStreamReader(
								socket.getInputStream()));
						String cmd;
						while (null != (cmd = reader.readLine())
								&& sockethandlingboolean[socketCount]) {
							addlog("got command: " + cmd);
							addlog("Command from: "
									+ socket.getRemoteSocketAddress());
							String result = commandHandler.processCommand(cmd);
							if (result == null) {
								addlog("send result: " + RESULT_OK);
								writer.write(RESULT_OK);
							} else {
								addlog("send result: " + result);
								writer.write(result);
							}
							writer.write("\n");
							writer.flush();
						}
						if (!sockethandlingboolean[socketCount]) {
							writer.write("kicked, new client");
							writer.write("\n");
							writer.flush();
							if (!socket.isClosed()) {
								addlog("Connection to "
										+ socket.getRemoteSocketAddress()
										+ " closed");
								socket.close();
							}
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).start();
		}

	}

	class TrayIcon {
		public TrayIcon() {
			new Thread(new Runnable() {
				public void run() {
					final Display display = new Display();
					final Shell shell = new Shell(display, SWT.DIALOG_TRIM
							| SWT.APPLICATION_MODAL);
					Image image = new Image(display,
							"/usr/share/icons/hicolor/24x24/apps/opreco_icon.png");
					final Tray tray = display.getSystemTray();

					ToolTip tip = new ToolTip(shell, SWT.BALLOON
							| SWT.ICON_INFORMATION);
					tip
							.setMessage("You can control you're opreco server from here now");

					if (tray == null) {
						addlog("The system tray is not available");
					} else {
						TrayItem item = new TrayItem(tray, SWT.NONE);
						item.setToolTip(tip);
						tip.setVisible(true);

						final Menu menu = new Menu(shell, SWT.POP_UP);

						MenuItem headline = new MenuItem(menu, SWT.PUSH);
						headline.setText("Java Remote Key Control Server");
						headline.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								Shell dialog = new Shell(shell);
								dialog.setSize(400, 200);
								dialog.setLayout(new FillLayout());

								Label label = new Label(dialog, SWT.NONE);
								label.setAlignment(SWT.CENTER);
								label
										.setText("Java - Remote Key Control Server\n "
												+ "A tool to control your OpenOffice presentations\n "
												+ "on your computer with your Jalimo and Linux powered\n "
												+ "cellphone via Bluetooth or LAN/W-Lan");
								dialog.open();
								while (!dialog.isDisposed()) {
									if (!display.readAndDispatch())
										display.sleep();
								}

							}
						});

						MenuItem seperator = new MenuItem(menu, SWT.SEPARATOR);

						MenuItem stop = new MenuItem(menu, SWT.CASCADE);
						stop.setText("Disconnect client");
						Menu subMenu = new Menu(menu);
						stop.setMenu(subMenu);
						subItem1 = new MenuItem(subMenu, SWT.PUSH);
						subItem1.setText("No client connected");
						subItem1.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								sockethandlingboolean[socketCount - 1] = false;
								subItem1.setText("No client connected");
							}
						});

						MenuItem clientHistory = new MenuItem(menu, SWT.PUSH);
						clientHistory.setText("Client history");
						clientHistory.addListener(SWT.Selection,
								new Listener() {
									public void handleEvent(Event event) {

										Shell dialog = new Shell(shell);
										dialog
												.setText("Remote Key Control - Client history");
										dialog.setSize(400, 500);
										dialog.setLayout(new FillLayout());

										List list = new List(dialog,
												SWT.V_SCROLL);
										for (int i = 0; i < clientList.size(); i++) {
											list.add(clientList.get(i));
										}

										dialog.open();
										while (!dialog.isDisposed()) {
											if (!display.readAndDispatch())
												display.sleep();
										}
									}
								});

						MenuItem log = new MenuItem(menu, SWT.PUSH);
						log.setText("View Log");
						log.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {

								Shell dialog = new Shell(shell);
								dialog.setText("Remote Key Control - Log");
								dialog.setSize(400, 500);
								dialog.setLayout(new FillLayout());

								List list = new List(dialog, SWT.V_SCROLL);
								for (int i = 0; i < logList.size(); i++) {
									list.add(logList.get(i));
								}

								dialog.open();
								while (!dialog.isDisposed()) {
									if (!display.readAndDispatch())
										display.sleep();
								}
							}
						});

						MenuItem separator2 = new MenuItem(menu, SWT.SEPARATOR);

						MenuItem quit = new MenuItem(menu, SWT.PUSH);
						quit.setText("Quit");
						quit.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								try {
									serverSocket.close();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								System.exit(1);
							}
						});

						final Shell dialog = new Shell(shell, SWT.DIALOG_TRIM
								| SWT.APPLICATION_MODAL);
						dialog.setText("Enter password for Login");
						dialog.setSize(350, 100);
						GridLayout dialogLayout = new GridLayout(3, false);
						dialogLayout.horizontalSpacing = 20;
						dialogLayout.verticalSpacing = 20;
						dialog.setLayout(dialogLayout);

						final Text password = new Text(dialog, SWT.NONE);

						password.setEchoChar('*');

						GridData passwordButtonGridData = new GridData(
								SWT.RIGHT, SWT.BOTTOM, false, false);
						passwordButtonGridData.widthHint = 170;
						password.setLayoutData(passwordButtonGridData);

						GridData buttonGridData = new GridData(SWT.RIGHT,
								SWT.BOTTOM, false, false);
						buttonGridData.widthHint = 50;

						Button enter = new Button(dialog, SWT.PUSH);
						enter.setText("Enter");
						enter.setLayoutData(buttonGridData);
						enter.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								SocketServerTransport.password = password
										.getText();
								dialog.dispose();
							}
						});

						Button exit = new Button(dialog, SWT.PUSH);
						exit.setText("Exit");
						exit.setLayoutData(buttonGridData);
						exit.addListener(SWT.Selection, new Listener() {
							public void handleEvent(Event event) {
								System.exit(1);
							}
						});

						/*
						 * if(getPasswordFromWallet() != null){
						 * password.setText(getPasswordFromWallet()); }
						 */

						final Button checkbox = new Button(dialog, SWT.CHECK);
						checkbox.setText("Save password");

						password.addKeyListener(new KeyAdapter() {
							public void keyPressed(KeyEvent e) {
								if (e.keyCode == SWT.CR) {
									SocketServerTransport.password = password
											.getText();
									if (checkbox.getSelection()) {
										// storePassword(password.getText());
									}
									dialog.dispose();
								}

							}
						});

						dialog.open();

						item.addListener(SWT.MenuDetect, new Listener() {
							public void handleEvent(Event event) {
								menu.setVisible(true);
							}
						});
						item.setImage(image);
					}
					while (!shell.isDisposed()) {
						if (!display.readAndDispatch())
							display.sleep();
					}

				}
			}).start();
		}
	}
}
