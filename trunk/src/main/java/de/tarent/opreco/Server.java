package de.tarent.opreco;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

public class Server implements ServerTransport.CommandHandler {

	Process xte;
	ServerTransport transport;
	OutputStreamWriter writer;
	BufferedReader reader;

	boolean noSWT = false;

	public Server(ServerTransport transport) {
		this.transport = transport;
	}

	public void start() {
		start(false);
	}

	public void start(boolean noSwt) {
		this.noSWT = noSwt;
		ProcessBuilder builder = new ProcessBuilder("xte");
		try {
			xte = builder.start();
			writer = new OutputStreamWriter(xte.getOutputStream());
			reader = new BufferedReader(new InputStreamReader(xte
					.getInputStream()));
		} catch (IOException e) {
			System.err
					.println("error starting 'xte', maybe you have to install the 'xautomation' package");
			System.err.println("e.g. sudo apt-get install xautomation");
			Shell shell = new Shell(Display.getCurrent());
			if (!noSwt) {
				MessageBox mb = new MessageBox(shell, SWT.ICON_WARNING);
				mb.setText("Missing Package");
				mb
						.setMessage("Package xautomation is missing.\nPlease get Package and try again.");
				mb.open();
			}
			System.exit(2);
		}

		try {
			transport.listen(this);
		} catch (IOException e) {
			System.out.println("error on listening");
			e.printStackTrace(System.err);
			System.exit(3);
		}

		xte.destroy();
	}

	public String processCommand(String cmd) throws IOException {
		if (Transport.SERVER_QUIT.equals(cmd)) {
			transport.close();
		}
		try {
			writer.write(cmd);
			writer.write("\n");
			writer.flush();
		} catch (IOException e) {
			System.out.println("trying to restart xte...");
			start(noSWT);
			writer.write(cmd);
			writer.write("\n");
			writer.flush();
		}
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// ignore safely
		}

		if (xte.getInputStream().available() > 0)
			return reader.readLine();
		else
			return null;
	}
}
